#[cfg(all(test, feature = "server"))]
mod tests {
    use exporter::server::{Server, ServerOptions};
    use std::env;
    use std::time::Duration;
    use tokio::time::sleep;

    async fn exit_after(duration: Duration) -> std::io::Result<()> {
        sleep(duration).await;
        Ok(())
    }

    #[tokio::test]
    async fn test_start_ok() {
        let port = env::var("EXPORTER_PORT").unwrap_or("8888".to_owned());

        let mut options = ServerOptions::default();
        options.port = port;

        let server = Server::new(options);

        let duration = Duration::from_secs(2);
        let exit = server.run(exit_after(duration)).await;

        assert!(exit.is_ok())
    }

    #[tokio::test]
    async fn test_start_port_in_use() {
        let port = env::var("EXPORTER_PORT").unwrap_or("8888".to_owned());

        let mut options = ServerOptions::default();
        options.port = port;

        let clone = options.clone();

        let server = Server::new(options);
        let server2 = Server::new(clone);

        let exit = tokio::spawn(async move {
            let duration = Duration::from_secs(5);
            server.run(exit_after(duration)).await
        });

        let second =
            tokio::spawn(async move { server2.run(exit_after(Duration::from_secs(2))).await });

        let (f, s) = tokio::join!(exit, second);
        assert!(f.unwrap().is_ok());
        assert!(s.unwrap().is_err())
    }
}
