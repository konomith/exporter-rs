#[cfg(all(test, feature = "server", feature = "client"))]
mod tests {
    use exporter::client::protocol::DefaultProtocolClient;
    use exporter::server::{Server, ServerOptions};

    use std::env;

    use bytes::BytesMut;
    use tokio::io::AsyncReadExt;
    use tokio::net::TcpListener;
    use tokio::sync::oneshot;
    use tokio::time::sleep;

    async fn exit(rx: oneshot::Receiver<()>) -> std::io::Result<()> {
        let _ = rx.await;
        Ok(())
    }

    async fn start(options: ServerOptions) -> oneshot::Sender<()> {
        let (tx, rx) = oneshot::channel::<()>();
        tokio::spawn(async move {
            let server = Server::new(options);
            let _ = server.run(exit(rx)).await;
        });
        tx
    }

    async fn tcp_listener_oneshot(tx: oneshot::Sender<u16>) -> std::io::Result<()> {
        let listener = TcpListener::bind("localhost:0").await?;
        let port = listener.local_addr()?.port();

        tx.send(port).expect("Unable to send port.");

        let (mut socket, _) = listener.accept().await?;
        let mut buf = BytesMut::with_capacity(8);
        socket.read_buf(&mut buf).await?;
        Ok(())
    }

    async fn destination_port_oneshot() -> u16 {
        let (port_sender, port_receiver) = oneshot::channel::<u16>();

        tokio::spawn(async move {
            tcp_listener_oneshot(port_sender)
                .await
                .expect("Error with listener")
        });

        let dest_port = port_receiver.await.expect("Couldn't get listener port");
        dest_port
    }

    #[tokio::test]
    async fn test_send_one() {
        let port = env::var("EXPORTER_PORT").unwrap_or("8888".to_owned());

        let mut options = ServerOptions::default();
        options.port = port.clone();

        let dest_port = destination_port_oneshot().await;

        let exiter = start(options).await;
        let client = DefaultProtocolClient::connect(&format!("localhost:{}", port))
            .await
            .expect("Client couldn't connect");
        let sent = client.send_tcp("localhost", dest_port, "data").await;

        assert!(sent.is_ok());

        let id = sent.expect("Couldn't retrieve packet id");
        sleep(std::time::Duration::from_secs(2)).await;
        let res = client
            .check_output(id)
            .await
            .expect("server didn't reply on time.");

        assert_eq!(res, String::from("OK"));

        let _ = exiter.send(());
    }
}
