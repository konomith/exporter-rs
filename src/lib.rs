#[cfg(feature = "server")]
#[macro_use]
extern crate lazy_static;

#[cfg(feature = "server")]
use git_version::git_version;

#[cfg(feature = "client")]
pub mod client;
#[cfg(feature = "server")]
pub mod container;
#[cfg(feature = "server")]
mod error;
#[cfg(feature = "server")]
mod handler;
#[cfg(any(feature = "server", feature = "client"))]
pub mod logging;
#[cfg(feature = "server")]
mod message;
#[cfg(any(feature = "server", feature = "client"))]
mod packet;
#[cfg(any(feature = "server", feature = "client"))]
mod parser;
#[cfg(feature = "server")]
mod pool;
#[cfg(feature = "server")]
pub mod server;
#[cfg(feature = "server")]
mod worker;

#[cfg(any(feature = "server", feature = "client"))]
#[macro_export]
macro_rules! version {
    () => {
        git_version!(
            args = ["--always", "--long", "--dirty=-mod"],
            cargo_suffix = "-mod-cargo",
            fallback = "unknown"
        )
    };
}
#[cfg(any(feature = "server", feature = "client"))]
pub const VERSION: &str = version!();
#[cfg(feature = "server")]
pub use server::{Server, ServerOptions};
