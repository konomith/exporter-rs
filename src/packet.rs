use std::collections::HashMap;
use std::convert::From;

use serde::{Deserialize, Serialize};
use serde_json;

pub type JsonParseResult<T> = Result<T, serde_json::Error>;

pub enum PacketType {
    WEBHOOK,
    TCP,
    UDP,
    EMAIL,
    INVALID = 999,
}

impl From<u64> for PacketType {
    fn from(v: u64) -> Self {
        match v {
            v if v == PacketType::WEBHOOK as u64 => PacketType::WEBHOOK,
            v if v == PacketType::TCP as u64 => PacketType::TCP,
            v if v == PacketType::UDP as u64 => PacketType::UDP,
            v if v == PacketType::EMAIL as u64 => PacketType::EMAIL,
            _ => PacketType::INVALID,
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Packet {
    pub r#type: u64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ProtocolPacket {
    pub r#type: u64,
    pub id: u64,
    pub addr: String,
    pub port: u16,
    pub payload: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct WebhookPacket {
    pub r#type: u64,
    pub id: u64,
    pub address: String,
    pub headers: HashMap<String, String>,
    pub payload: String,
    pub auth_required: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EmailPacket {
    pub r#type: u64,
    pub id: u64,
    pub email: String,
    pub subject: String,
    pub payload: String,
    pub attachment: Option<Vec<String>>,
    pub attachment_name: Option<Vec<String>>,
}

pub fn parse_packet(raw: &str) -> PacketType {
    let p: Packet = serde_json::from_str(raw).unwrap_or(Packet { r#type: 999 });
    PacketType::from(p.r#type)
}

pub fn parse_protocol_packet(raw: &str) -> JsonParseResult<ProtocolPacket> {
    let p: ProtocolPacket = serde_json::from_str(raw)?;
    Ok(p)
}

pub fn parse_webhook_packet(raw: &str) -> JsonParseResult<WebhookPacket> {
    let p: WebhookPacket = serde_json::from_str(raw)?;
    Ok(p)
}

pub fn parse_email_packet(raw: &str) -> JsonParseResult<EmailPacket> {
    let p: EmailPacket = serde_json::from_str(raw)?;
    Ok(p)
}
