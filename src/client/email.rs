use std::fs;
use std::io::{Error as StdIoError, ErrorKind as StdErrorKind};
use std::path::Path;

use serde_json;
use tokio::net::ToSocketAddrs;

use super::util::encode_attachment;
use super::{Client, ExporterClient};
use crate::packet::{EmailPacket, PacketType};

#[derive(Debug, Clone)]
pub struct EmailClient<T: Client> {
    client: T,
}

impl<T: Client> EmailClient<T> {
    pub fn with_client(client: T) -> Self {
        Self { client }
    }

    pub async fn connect<A: ToSocketAddrs + Send>(addr: A) -> Result<Self, StdIoError> {
        let client = T::connect(addr).await?;

        Ok(Self { client })
    }

    pub async fn send<P: AsRef<Path>>(
        &self,
        email_address: &str,
        subject: &str,
        body: &str,
        attachments: Option<&Vec<P>>,
    ) -> Result<u64, StdIoError> {
        let id = self.client.next_id();

        let mut attachments_encoded = None;
        let mut attachments_names = None;

        if let Some(attachments) = attachments {
            let mut payloads = Vec::with_capacity(attachments.len());
            let mut fnames = Vec::with_capacity(attachments.len());

            for path in attachments.iter() {
                let (fname, encoded) = self.prepare_attachment(&path)?;
                payloads.push(encoded);
                fnames.push(fname);
            }

            attachments_encoded = Some(payloads);
            attachments_names = Some(fnames);
        }

        let packet = EmailPacket {
            id,
            r#type: PacketType::EMAIL as u64,
            email: email_address.to_owned(),
            subject: subject.to_owned(),
            payload: body.to_owned(),
            attachment: attachments_encoded,
            attachment_name: attachments_names,
        };

        let as_json = serde_json::to_string(&packet)
            .or_else(|e| Err(StdIoError::new(StdErrorKind::Other, e.to_string())))?;
        self.client.send(as_json).await?;
        Ok(id)
    }

    pub async fn check_output(&self, id: u64) -> Option<String> {
        let res = self.client.result(id).await;
        match res {
            None => None,
            Some(s) => match &*s {
                "0x00" => Some("OK".to_owned()),
                "0x01" => Some("Failed to send".to_owned()),
                "0x02" => Some("Send, error in attachment".to_owned()),
                _ => Some("Failed to send".to_owned()),
            },
        }
    }

    pub fn broken(&self) -> bool {
        self.client.broken()
    }

    fn prepare_attachment<P: AsRef<Path>>(&self, path: &P) -> Result<(String, String), StdIoError> {
        let content = fs::read(path)?;
        let encoded = encode_attachment(&content)?;
        let fname = path
            .as_ref()
            .file_name()
            .ok_or(StdIoError::new(StdErrorKind::Other, "Invalid path"))?
            .to_owned()
            .into_string()
            .or(Err(StdIoError::new(
                StdErrorKind::Other,
                "Invalid filename",
            )))?;
        Ok((fname, encoded))
    }
}

pub type DefaultEmailClient = EmailClient<ExporterClient>;

#[cfg(test)]
mod tests {
    use std::io::{Error as StdIoError, Write};

    use tempfile::NamedTempFile;

    use super::super::tests::MockClient;
    use super::EmailClient;

    #[test]
    fn test_prepare_attachment() {
        let mut f = NamedTempFile::new().unwrap();
        let _ = f.write_all(b"data");

        let c = MockClient::new();
        let ec = EmailClient::with_client(c);

        let res = ec.prepare_attachment(&f);
        assert!(res.is_ok());

        let fname = res.unwrap().0;
        let temp_fname = f.path().file_name().unwrap().to_str().unwrap();
        assert_eq!(temp_fname, &fname)
    }

    #[test]
    fn test_prepare_attachment_error() {
        let c = MockClient::new();
        let ec = EmailClient::with_client(c);

        let res = ec.prepare_attachment(&"path/that/doesnt/exist/at/all");
        assert!(res.is_err())
    }

    #[tokio::test]
    async fn test_send_ok() {
        let mock = MockClient::new().with_return_value(String::from("0x00"));
        let wc: EmailClient<MockClient> = EmailClient::with_client(mock);
        let res = wc
            .send::<&str>("test@localhost", "subject", "body", None)
            .await;
        assert!(res.is_ok())
    }

    #[tokio::test]
    async fn test_send_error() {
        let mock = MockClient::new().with_side_effect(StdIoError::new(
            std::io::ErrorKind::Other,
            "failed: permanent",
        ));
        let wc: EmailClient<MockClient> = EmailClient::with_client(mock);
        let res = wc
            .send::<&str>("test@localhost", "subject", "body", None)
            .await;
        assert!(res.is_err());
        assert!(wc.broken());
    }
}
