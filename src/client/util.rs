use std::io::prelude::*;
use std::io::Error as StdIoError;
use std::str::from_utf8;

use base64;
use flate2::{write::GzEncoder, Compression};

pub(crate) fn encode_attachment(payload: &[u8]) -> Result<String, StdIoError> {
    let mut compressor = GzEncoder::new(Vec::new(), Compression::default());
    compressor.write_all(payload)?;

    let compressed = compressor.finish()?;
    let encoded = base64::encode(&compressed);
    Ok(encoded)
}

pub(crate) fn parse_response(data: &[u8], default: u64) -> (u64, String) {
    let result = from_utf8(data).unwrap_or("");
    let mut split = result.split('|');
    let id = match split.next() {
        Some(id) => {
            if id == "-1" {
                default
            } else {
                id.parse::<u64>().unwrap_or(default)
            }
        }
        None => default,
    };
    let res = split.next().unwrap_or("0x01");
    (id, res.to_owned())
}
