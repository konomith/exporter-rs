use std::io::{Error as StdIoError, ErrorKind as StdErrorKind};

use serde_json;
use tokio::net::ToSocketAddrs;

use super::{Client, ExporterClient};
use crate::packet::{PacketType, ProtocolPacket};

#[derive(Debug, Clone)]
pub struct ProtocolClient<T: Client> {
    client: T,
}

impl<T: Client> ProtocolClient<T> {
    pub fn with_client(client: T) -> Self {
        Self { client }
    }

    pub async fn connect<A: ToSocketAddrs + Send>(addr: A) -> Result<Self, StdIoError> {
        let client = T::connect(addr).await?;

        Ok(Self { client })
    }

    async fn send(
        &self,
        addr: &str,
        port: u16,
        payload: &str,
        ptype: PacketType,
    ) -> Result<u64, StdIoError> {
        let packet_type = match ptype {
            PacketType::UDP => PacketType::UDP as u64,
            _ => PacketType::TCP as u64,
        };

        let id = self.client.next_id();

        let packet = ProtocolPacket {
            id,
            port,
            addr: addr.to_owned(),
            r#type: packet_type,
            payload: payload.to_owned(),
        };

        let as_json = serde_json::to_string(&packet)
            .or_else(|e| Err(StdIoError::new(StdErrorKind::Other, e.to_string())))?;
        self.client.send(as_json).await?;
        Ok(id)
    }

    pub async fn send_tcp(&self, addr: &str, port: u16, payload: &str) -> Result<u64, StdIoError> {
        self.send(addr, port, payload, PacketType::TCP).await
    }

    pub async fn send_udp(&self, addr: &str, port: u16, payload: &str) -> Result<u64, StdIoError> {
        self.send(addr, port, payload, PacketType::UDP).await
    }

    pub async fn check_output(&self, id: u64) -> Option<String> {
        let res = self.client.result(id).await;
        match res {
            None => None,
            Some(s) => match &*s {
                "0x00" => Some("OK".to_owned()),
                "0x01" => Some("Destination unreachable".to_owned()),
                "0x02" => Some("Failed to deliver".to_owned()),
                _ => Some("Failed to deliver".to_owned()),
            },
        }
    }

    pub fn broken(&self) -> bool {
        self.client.broken()
    }
}

pub type DefaultProtocolClient = ProtocolClient<ExporterClient>;

#[cfg(test)]
mod tests {
    use std::io::Error as StdIoError;

    use super::super::tests::MockClient;
    use super::{PacketType, ProtocolClient};

    #[tokio::test]
    async fn test_send_ok() {
        let mock = MockClient::new().with_return_value(String::from("0x00"));
        let wc: ProtocolClient<MockClient> = ProtocolClient::with_client(mock);
        let res = wc.send("localhost", 9001, "data", PacketType::TCP).await;
        assert!(res.is_ok())
    }

    #[tokio::test]
    async fn test_send_error() {
        let mock = MockClient::new().with_side_effect(StdIoError::new(
            std::io::ErrorKind::Other,
            "failed: permanent",
        ));
        let wc: ProtocolClient<MockClient> = ProtocolClient::with_client(mock);
        let res = wc.send("localhost", 9001, "data", PacketType::TCP).await;
        assert!(res.is_err());
        assert!(wc.broken());
    }

    #[tokio::test]
    async fn test_send_error_temporary() {
        let mock = MockClient::new()
            .with_side_effect(StdIoError::new(std::io::ErrorKind::Other, "failed"));
        let wc: ProtocolClient<MockClient> = ProtocolClient::with_client(mock);
        let res = wc.send("localhost", 9001, "data", PacketType::TCP).await;
        assert!(res.is_err());
        assert!(!wc.broken());
    }

    #[tokio::test]
    async fn test_send_tcp() {
        let mock = MockClient::new().with_return_value(String::from("0x00"));
        let wc: ProtocolClient<MockClient> = ProtocolClient::with_client(mock);
        let res = wc.send_tcp("localhost", 9001, "data").await;
        assert!(res.is_ok())
    }

    #[tokio::test]
    async fn test_send_udp() {
        let mock = MockClient::new().with_return_value(String::from("0x00"));
        let wc: ProtocolClient<MockClient> = ProtocolClient::with_client(mock);
        let res = wc.send_udp("localhost", 9001, "data").await;
        assert!(res.is_ok())
    }
}
