use std::collections::HashMap;
use std::io::{Error as StdIoError, ErrorKind as StdErrorKind};

use serde_json;
use tokio::net::ToSocketAddrs;

use super::{Client, ExporterClient};
use crate::packet::{PacketType, WebhookPacket};

#[derive(Debug, Clone)]
pub struct WebhookClient<T: Client> {
    client: T,
}

impl<T: Client> WebhookClient<T> {
    pub fn with_client(client: T) -> Self {
        Self { client }
    }

    pub async fn connect<A: ToSocketAddrs + Send>(addr: A) -> Result<Self, StdIoError> {
        let client = T::connect(addr).await?;

        Ok(Self { client })
    }

    pub async fn send(
        &self,
        address: &str,
        headers: &HashMap<String, String>,
        payload: &str,
        auth_required: bool,
    ) -> Result<u64, StdIoError> {
        let id = self.client.next_id();

        let packet = WebhookPacket {
            id,
            r#type: PacketType::WEBHOOK as u64,
            address: address.to_owned(),
            payload: payload.to_owned(),
            headers: headers.to_owned(),
            auth_required,
        };

        let as_json = serde_json::to_string(&packet)
            .or_else(|e| Err(StdIoError::new(StdErrorKind::Other, e.to_string())))?;
        self.client.send(as_json).await?;
        Ok(id)
    }

    pub async fn check_output(&self, id: u64) -> Option<String> {
        let res = self.client.result(id).await;
        match res {
            None => None,
            Some(s) => match &*s {
                "0x00" => Some("OK".to_owned()),
                "0x01" => Some("Error".to_owned()),
                "0x02" => Some("Destination port out of range".to_owned()),
                _ => Some("Error".to_owned()),
            },
        }
    }

    pub fn broken(&self) -> bool {
        self.client.broken()
    }
}

pub type DefaultWebhookClient = WebhookClient<ExporterClient>;

#[cfg(test)]
mod tests {
    use std::collections::HashMap;
    use std::io::Error as StdIoError;

    use super::super::tests::MockClient;
    use super::WebhookClient;

    #[tokio::test]
    async fn test_send_ok() {
        let mock = MockClient::new().with_return_value(String::from("0x00"));
        let wc: WebhookClient<MockClient> = WebhookClient::with_client(mock);
        let res = wc
            .send("http://localhost:9001", &HashMap::new(), "body", false)
            .await;
        assert!(res.is_ok())
    }

    #[tokio::test]
    async fn test_send_error() {
        let mock = MockClient::new().with_side_effect(StdIoError::new(
            std::io::ErrorKind::Other,
            "failed: permanent",
        ));
        let wc: WebhookClient<MockClient> = WebhookClient::with_client(mock);
        let res = wc
            .send("http://localhost:9001", &HashMap::new(), "body", false)
            .await;
        assert!(res.is_err());
        assert!(wc.broken());
    }

    #[tokio::test]
    async fn test_send_error_temporary() {
        let mock = MockClient::new()
            .with_side_effect(StdIoError::new(std::io::ErrorKind::Other, "failed"));
        let wc: WebhookClient<MockClient> = WebhookClient::with_client(mock);
        let res = wc
            .send("http://localhost:9001", &HashMap::new(), "body", false)
            .await;
        assert!(res.is_err());
        assert!(!wc.broken());
    }
}
