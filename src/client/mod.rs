use std::collections::HashMap;
use std::io::{Error as StdIoError, ErrorKind as StdErrorKind};
use std::marker::PhantomData;
use std::sync::{
    atomic::{AtomicBool, AtomicU64, Ordering},
    Arc,
};

use async_trait::async_trait;
use bytes::BytesMut;
use log;
use tokio::io::{AsyncRead, AsyncWrite, AsyncWriteExt};
use tokio::{
    net::{
        tcp::{OwnedReadHalf, OwnedWriteHalf},
        TcpStream, ToSocketAddrs,
    },
    sync::{
        mpsc::{unbounded_channel, UnboundedReceiver},
        Mutex, RwLock,
    },
};
use tokio_stream::StreamExt;
use tokio_util::codec::{AnyDelimiterCodec, Decoder, Encoder, FramedRead, LinesCodec};

pub mod email;
pub mod protocol;
pub mod webhook;

mod util;

#[async_trait]
trait Receiver {
    async fn recv(&mut self) -> Result<Vec<u8>, StdIoError>;
}

#[async_trait]
trait Sender: Send + Sync {
    type Payload;
    fn encode(&mut self, payload: Self::Payload) -> Option<Vec<u8>>;
    async fn send_raw(&mut self, payload: &[u8]) -> Result<(), StdIoError>;
}

#[derive(Debug)]
pub(crate) struct ResponseReader<T, D>
where
    T: AsyncRead + Unpin + Send,
    D: Decoder,
{
    reader: FramedRead<T, D>,
}

impl<T, D> ResponseReader<T, D>
where
    T: AsyncRead + Unpin + Send,
    D: Decoder,
{
    fn new(client: T, decoder: D) -> Self {
        let reader = FramedRead::with_capacity(client, decoder, 256);
        ResponseReader { reader }
    }
}

#[async_trait]
impl<T, D> Receiver for ResponseReader<T, D>
where
    T: AsyncRead + Unpin + Send,
    D: Decoder + Send,
    <D as Decoder>::Item: AsRef<[u8]> + Send + Sync,
    <D as Decoder>::Error: std::error::Error + Send + Sync,
{
    async fn recv(&mut self) -> Result<Vec<u8>, StdIoError> {
        let frame = self
            .reader
            .next()
            .await
            .ok_or_else(|| StdIoError::new(StdErrorKind::Other, "No frame"))?;
        match frame {
            Ok(data) => Ok(data.as_ref().to_vec()),
            Err(e) => Err(StdIoError::new(StdErrorKind::Other, e.to_string())),
        }
    }
}

#[derive(Debug)]
pub(crate) struct PacketSender<T, E, R>
where
    T: AsyncWrite + Unpin + Send + Sync,
    E: Encoder<R> + Send + Sync,
    R: AsRef<str> + Send + Sync,
{
    sender: T,
    encoder: E,
    _marker: PhantomData<R>,
}

impl<T, E, R> PacketSender<T, E, R>
where
    T: AsyncWrite + Unpin + Send + Sync,
    E: Encoder<R> + Send + Sync,
    R: AsRef<str> + Send + Sync,
{
    fn new(sender: T, encoder: E) -> Self {
        PacketSender {
            sender,
            encoder,
            _marker: PhantomData,
        }
    }
}

#[async_trait]
impl<T, E, R> Sender for PacketSender<T, E, R>
where
    T: AsyncWrite + Unpin + Send + Sync,
    E: Encoder<R> + Send + Sync,
    R: AsRef<str> + Send + Sync,
{
    type Payload = R;
    fn encode(&mut self, payload: Self::Payload) -> Option<Vec<u8>> {
        let len = payload.as_ref().len();
        let mut buf = BytesMut::with_capacity(len + 2);
        match self.encoder.encode(payload, &mut buf) {
            Ok(_) => Some(buf.to_vec()),
            Err(_) => None,
        }
    }

    async fn send_raw(&mut self, payload: &[u8]) -> Result<(), StdIoError> {
        let _ = self.sender.write_all(payload).await?;
        Ok(())
    }
}

#[async_trait]
trait Transport {
    type Connection;
    type Reader;
    type Writer;
    type Error;

    async fn connect<A: ToSocketAddrs + Send>(addr: A) -> Result<Self::Connection, Self::Error>;
}

#[derive(Debug, Clone)]
struct TcpTransport;

#[async_trait]
impl Transport for TcpTransport {
    type Connection = TcpStream;
    type Reader = OwnedReadHalf;
    type Writer = OwnedWriteHalf;
    type Error = StdIoError;

    async fn connect<A: ToSocketAddrs + Send>(addr: A) -> Result<Self::Connection, Self::Error> {
        TcpStream::connect(addr).await
    }
}

#[derive(Debug)]
enum Response {
    Data(Vec<u8>),
    Err(StdIoError),
}

impl Response {
    fn is_err(&self) -> bool {
        match *self {
            Self::Err(_) => true,
            _ => false
        }
    }
}

#[derive(Debug, Clone)]
struct RawClient<T, E, D, R>
where
    T: Transport,
    E: Encoder<R> + Send + Sync,
    D: Decoder,
    R: AsRef<str> + Send + Sync,
    <T as Transport>::Reader: AsyncRead + Unpin + Send + Sync,
    <T as Transport>::Writer: AsyncWrite + Unpin + Send + Sync,
{
    // transport: T,
    reader: Arc<Mutex<ResponseReader<T::Reader, D>>>,
    writer: Arc<Mutex<PacketSender<T::Writer, E, R>>>,
    _marker: PhantomData<R>,
}

impl<E, D, R> RawClient<TcpTransport, E, D, R>
where
    E: Encoder<R> + Send + Sync,
    D: Decoder + Send + Sync + 'static,
    R: AsRef<str> + Send + Sync,
    <TcpTransport as Transport>::Reader: AsyncRead + Unpin + Send + Sync,
    <TcpTransport as Transport>::Writer: AsyncWrite + Unpin + Send + Sync,
    <D as Decoder>::Item: AsRef<[u8]> + Send + Sync,
    <D as Decoder>::Error: std::error::Error + Send + Sync,
{
    async fn new<A: ToSocketAddrs + Send>(
        addr: A,
        encoder: E,
        decoder: D,
    ) -> Result<Self, StdIoError> {
        let conn = TcpTransport::connect(addr).await?;
        let (read, write) = conn.into_split();
        let reader = Arc::new(Mutex::new(ResponseReader::new(read, decoder)));
        let writer = Arc::new(Mutex::new(PacketSender::new(write, encoder)));
        let client = RawClient {
            // transport: TcpTransport,
            reader,
            writer,
            _marker: PhantomData,
        };
        Ok(client)
    }

    async fn send(&self, payload: R) -> Result<(), StdIoError> {
        let mut sender = self.writer.lock().await;
        let packet = sender
            .encode(payload)
            .ok_or(StdIoError::new(StdErrorKind::Other, "Invalid payload"))?;
        sender.send_raw(&packet).await
    }

    async fn init_receiver(&self) -> UnboundedReceiver<Response> {
        let (tx, rx) = unbounded_channel::<Response>();
        let reader = self.reader.clone();
        tokio::spawn(async move {
            let mut guard = reader.lock().await;
            loop {
                let packet = match guard.recv().await {
                    Ok(res) => Response::Data(res),
                    Err(e) => Response::Err(e),
                };

                log::debug!("init_receiver: packet: {:?}", packet);

                let is_err = packet.is_err();
                let _ = tx.send(packet);
                if is_err {
                    break;
                }
            }
        });
        rx
    }
}

type ExporterTcpRawClient = RawClient<TcpTransport, AnyDelimiterCodec, LinesCodec, String>;

#[async_trait]
pub trait Client {
    async fn connect<A: ToSocketAddrs + Send>(addr: A) -> Result<Self, StdIoError>
    where
        Self: Sized;
    async fn send(&self, payload: String) -> Result<(), StdIoError>;
    async fn result(&self, id: u64) -> Option<String>;
    fn broken(&self) -> bool;
    fn next_id(&self) -> u64;
}

#[derive(Debug, Clone)]
pub struct ExporterClient {
    raw: Arc<ExporterTcpRawClient>,
    responses: Arc<RwLock<HashMap<u64, String>>>,
    broken: Arc<AtomicBool>,
    pid: Arc<AtomicU64>,
}

impl ExporterClient {
    async fn init_receiver(&self) {
        let mut rx = self.raw.init_receiver().await;

        let broken = self.broken.clone();
        let responses = self.responses.clone();
        let pid = self.pid.clone();

        tokio::spawn(async move {
            loop {
                if broken.load(Ordering::SeqCst) {
                    log::debug!("Connection broken, breaking receiver loop");
                    break;
                }
                match rx.recv().await {
                    Some(response) => match response {
                        Response::Data(payload) => {
                            let (id, result) =
                                util::parse_response(&payload, pid.load(Ordering::SeqCst));
                            let _ = responses.write().await.insert(id, result);
                        }
                        Response::Err(e) => {
                            log::warn!("Error: {}", e);
                            broken.store(true, Ordering::SeqCst);
                        }
                    },
                    None => broken.store(true, Ordering::SeqCst),
                }
            }
        });
    }
}

#[async_trait]
impl Client for ExporterClient {
    async fn connect<A: ToSocketAddrs + Send>(addr: A) -> Result<Self, StdIoError> {
        let encoder = AnyDelimiterCodec::new(b"".to_vec(), b"\0".to_vec());
        let decoder = LinesCodec::new_with_max_length(256);
        let raw = ExporterTcpRawClient::new(addr, encoder, decoder).await?;

        let c = Self {
            raw: Arc::new(raw),
            responses: Arc::new(RwLock::new(HashMap::new())),
            broken: Arc::new(AtomicBool::new(false)),
            pid: Arc::new(AtomicU64::new(0)),
        };

        c.init_receiver().await;
        Ok(c)
    }

    async fn send(&self, payload: String) -> Result<(), StdIoError> {
        self.raw.send(payload).await
    }

    async fn result(&self, id: u64) -> Option<String> {
        match self.responses.read().await.get(&id) {
            Some(s) => Some(s.clone()),
            None => None,
        }
    }

    fn broken(&self) -> bool {
        self.broken.load(Ordering::SeqCst)
    }

    fn next_id(&self) -> u64 {
        self.pid.fetch_add(1, Ordering::SeqCst)
    }
}

#[cfg(test)]
#[allow(dead_code)]
#[doc(hidden)]
pub(crate) mod tests {
    use std::io::Error as StdIoError;

    use async_trait::async_trait;
    use tokio::net::ToSocketAddrs;

    use super::Client;

    pub struct MockClient {
        pub return_value: Option<String>,
        pub side_effect: Option<StdIoError>,
    }

    impl MockClient {
        pub fn new() -> Self {
            MockClient {
                return_value: None,
                side_effect: None,
            }
        }

        pub fn with_return_value(mut self, v: String) -> Self {
            self.return_value = Some(v);
            self
        }

        pub fn with_side_effect(mut self, e: StdIoError) -> Self {
            self.side_effect = Some(e);
            self
        }
    }

    #[async_trait]
    impl Client for MockClient {
        async fn connect<A: ToSocketAddrs + Send>(_addr: A) -> Result<Self, StdIoError>
        where
            Self: Sized,
        {
            Ok(MockClient::new())
        }

        async fn send(&self, _payload: String) -> Result<(), StdIoError> {
            if let Some(ref err) = self.side_effect {
                return Err(StdIoError::new(err.kind(), err.to_string()));
            }
            Ok(())
        }

        async fn result(&self, _id: u64) -> Option<String> {
            self.return_value.clone()
        }

        fn broken(&self) -> bool {
            if let Some(ref err) = self.side_effect {
                return err.to_string().contains("permanent");
            }
            false
        }

        fn next_id(&self) -> u64 {
            1
        }
    }
}
