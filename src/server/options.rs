use std::env;
use std::error::Error;
use std::fs;
use std::path::Path;

use clap;
use serde::Deserialize;
use toml;

lazy_static! {
    static ref ENV_REGEX: regex::Regex = regex::Regex::new(r"^\$\{ENV_(?P<var>\w*)\}$").unwrap();
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename = "email")]
pub struct EmailOptions {
    pub server: String,
    pub port: u16,
    pub username: String,
    pub password: String,
    pub auth: bool,
    pub tls: bool,
    pub skip_tls_verify: bool,
    pub from: String
}

impl Default for EmailOptions {
    fn default() -> Self {
        EmailOptions {
            server: String::from("127.0.0.1"),
            port: 1025,
            username: String::from("exporter@localhost"),
            password: String::from(""),
            auth: false,
            tls: false,
            skip_tls_verify: false,
            from: String::from("Exporter <exporter@localhost>")
        }
    }
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename = "exporter")]
pub struct ServerOptions {
    pub host: String,
    pub port: String,
    pub no_wait: bool,
    pub tcp_workers: u32,
    pub tcp_pool_size: u32,
    pub tcp_connect_timeout: u64,
    pub tcp_idle_timeout: u64,
    pub webhook_connect_timeout: u64,
    pub webhook_skip_cert_verify: bool,
    pub webhook_workers: u32,
    pub udp_workers: u32,
    pub udp_pool_size: u32,
    pub udp_idle_timeout: u64,
    pub email_workers: u32,
    pub task_buff_size: usize,
    pub max_packet_size: usize,
    pub read_buffer_size: usize,
    pub oauth_server: String,
    pub oauth_realm: String,
    pub oauth_client_id: String,
    pub oauth_client_secret: String,
    #[serde(rename = "email")]
    pub email_options: EmailOptions,
}

impl Default for ServerOptions {
    fn default() -> Self {
        ServerOptions {
            host: String::from("0.0.0.0"),
            port: String::from("8888"),
            no_wait: false,
            tcp_workers: 10u32,
            tcp_pool_size: 10u32,
            tcp_connect_timeout: 1000u64, // in milliseconds
            tcp_idle_timeout: 600u64,     // in seconds
            task_buff_size: 1024 * 128,
            max_packet_size: 1024 * 1024 * 64 * 2, // in bytes
            read_buffer_size: 1024 * 64,           // in bytes
            oauth_server: String::from("http://localhost:8080"),
            oauth_realm: String::from("tinaa"),
            oauth_client_id: String::from("exporter"),
            oauth_client_secret: String::from(""),
            webhook_connect_timeout: 2000u64, // in milliseconds
            webhook_workers: 10u32,
            webhook_skip_cert_verify: false,
            udp_workers: 10u32,
            udp_pool_size: 1u32,
            udp_idle_timeout: 600u64, // in seconds
            email_workers: 10u32,
            email_options: EmailOptions::default(),
        }
    }
}

impl ServerOptions {
    pub fn from_matches(args: &clap::ArgMatches<'static>) -> Self {
        if let Some(path) = args.value_of("config") {
            match Self::from_file(path) {
                Ok(opts) => {
                    return opts;
                }
                Err(e) => {
                    eprintln!("[WARN] Couldn't parse config file: {}", e);
                    eprintln!("[WARN] Continuing with default values...");
                    return ServerOptions::default();
                }
            }
        }

        let d: ServerOptions = Default::default();
        let default_email = EmailOptions::default();

        let email = EmailOptions {
            server: args
                .value_of("email-server")
                .unwrap_or(&default_email.server)
                .to_string(),
            port: args
                .value_of("email-port")
                .unwrap_or(&default_email.port.to_string())
                .parse()
                .unwrap_or(default_email.port),
            username: args
                .value_of("email-username")
                .unwrap_or(&default_email.username)
                .to_string(),
            password: args
                .value_of("email-password")
                .unwrap_or(&default_email.password)
                .to_string(),
            from: args
                .value_of("email-from")
                .unwrap_or(&default_email.from)
                .to_string(),
            auth: args.is_present("email-auth"),
            tls: args.is_present("email-tls"),
            skip_tls_verify: args.is_present("email-skip-tls-verify"),
        };

        ServerOptions {
            host: args.value_of("host").unwrap_or(&d.host).to_string(),
            port: args.value_of("port").unwrap_or(&d.port).to_string(),
            no_wait: args.is_present("no-wait"),
            tcp_workers: args
                .value_of("tcp-workers")
                .unwrap_or(&d.tcp_workers.to_string())
                .parse()
                .unwrap_or(d.tcp_workers),
            tcp_pool_size: args
                .value_of("tcp-pool-size")
                .unwrap_or(&d.tcp_pool_size.to_string())
                .parse()
                .unwrap_or(d.tcp_pool_size),
            tcp_connect_timeout: args
                .value_of("tcp-connect-timeout")
                .unwrap_or(&d.tcp_connect_timeout.to_string())
                .parse()
                .unwrap_or(d.tcp_connect_timeout),
            tcp_idle_timeout: args
                .value_of("tcp-idle-timeout")
                .unwrap_or(&d.tcp_idle_timeout.to_string())
                .parse()
                .unwrap_or(d.tcp_idle_timeout),
            task_buff_size: args
                .value_of("task-buff-size")
                .unwrap_or(&d.task_buff_size.to_string())
                .parse()
                .unwrap_or(d.task_buff_size),
            max_packet_size: args
                .value_of("max-packet-size")
                .unwrap_or(&d.max_packet_size.to_string())
                .parse()
                .unwrap_or(d.max_packet_size),
            read_buffer_size: args
                .value_of("read-buffer-size")
                .unwrap_or(&d.read_buffer_size.to_string())
                .parse()
                .unwrap_or(d.read_buffer_size),
            oauth_server: args
                .value_of("oauth-server")
                .unwrap_or(&d.oauth_server)
                .to_string(),
            oauth_realm: args
                .value_of("oauth-realm")
                .unwrap_or(&d.oauth_realm)
                .to_string(),
            oauth_client_id: args
                .value_of("oauth-client-id")
                .unwrap_or(&d.oauth_client_id)
                .to_string(),
            oauth_client_secret: args
                .value_of("oauth-client-secret")
                .unwrap_or(&d.oauth_client_secret)
                .to_string(),
            webhook_connect_timeout: args
                .value_of("webhook-connect-timeout")
                .unwrap_or(&d.webhook_connect_timeout.to_string())
                .parse()
                .unwrap_or(d.webhook_connect_timeout),
            webhook_workers: args
                .value_of("webhook-workers")
                .unwrap_or(&d.webhook_workers.to_string())
                .parse()
                .unwrap_or(d.webhook_workers),
            webhook_skip_cert_verify: args.is_present("webhook-skip-cert-verify"),
            udp_workers: args
                .value_of("udp-workers")
                .unwrap_or(&d.udp_workers.to_string())
                .parse()
                .unwrap_or(d.udp_workers),
            udp_pool_size: args
                .value_of("udp-pool-size")
                .unwrap_or(&d.udp_pool_size.to_string())
                .parse()
                .unwrap_or(d.udp_pool_size),
            udp_idle_timeout: args
                .value_of("udp-idle-timeout")
                .unwrap_or(&d.udp_idle_timeout.to_string())
                .parse()
                .unwrap_or(d.udp_idle_timeout),
            email_workers: args
                .value_of("email-workers")
                .unwrap_or(&d.email_workers.to_string())
                .parse()
                .unwrap_or(d.email_workers),
            email_options: email,
        }
    }

    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Self, Box<dyn Error>> {
        let mut content = fs::read_to_string(path)?;

        let mut store = content.parse::<toml::Value>()?;
        replace_from_env(&mut store);
        content = toml::to_string(&store)?;

        let options: ServerOptions = toml::from_str(&content)?;
        Ok(options)
    }
}

fn get_env_key(entry: &str) -> Option<String> {
    match ENV_REGEX.captures(entry) {
        Some(captures) => match captures.name("var") {
            Some(cap) => Some(cap.as_str().to_owned()),
            None => None,
        },
        None => None,
    }
}

fn get_env_val(entry: &str) -> String {
    env::var(entry).unwrap_or("".to_owned())
}

fn replace_from_env(store: &mut toml::Value) {
    if let Some(as_table) = store.as_table_mut() {
        for (_, val) in as_table.iter_mut() {
            if val.is_table() {
                replace_from_env(val);
            } else if val.is_str() {
                let v = val.as_str().unwrap();
                match get_env_key(v) {
                    Some(env_key) => {
                        let env_val = get_env_val(&env_key);
                        *val = toml::Value::String(env_val);
                    }
                    None => {}
                }
            }
        }
    }
}
