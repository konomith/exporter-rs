use std::future::Future;
use std::io;
use std::sync::{Arc, RwLock};

use log;
use tokio::net;
use tokio::sync::mpsc;

use crate::container::MutexConnectionContainer;
use crate::handler::*;
use crate::message::Message;
use crate::server::options::ServerOptions;
use crate::worker::*;

#[derive(Debug)]
pub struct Server {
    opts: ServerOptions,
    listener: Option<net::TcpListener>,
    exit: mpsc::Receiver<()>,
    conns: MutexConnectionContainer<String, net::tcp::OwnedWriteHalf>,
    write_worker: WriteWorker,
    read_worker: ReadWorker,
    result_worker: ResultWorker,
}

impl Server {
    pub fn new(opts: ServerOptions) -> Server {
        let (tx, rx) = mpsc::channel(opts.task_buff_size);
        let (res_tx, res_rx) = mpsc::channel(opts.task_buff_size);
        let (exit_tx, exit_rx) = mpsc::channel(1);
        let conns = MutexConnectionContainer::new();

        let arc_opts = Arc::new(opts.clone());

        let ww = WriteWorker::new(
            res_tx.clone(),
            Arc::new(RwLock::new(Some(rx))),
            arc_opts.clone(),
        );

        let rw = ReadWorker { ts: tx };

        let resw = ResultWorker::new(
            Arc::new(RwLock::new(Some(res_rx))),
            conns.clone(),
            exit_tx,
        );

        Server {
            opts: opts.clone(),
            listener: None,
            conns,
            write_worker: ww,
            read_worker: rw,
            result_worker: resw,
            exit: exit_rx,
        }
    }

    async fn init_listener(&mut self) -> Result<(), io::Error> {
        let address = format!("{}:{}", self.opts.host, self.opts.port);
        let listener = net::TcpListener::bind(address).await?;

        self.listener = Some(listener);
        Ok(())
    }

    async fn get_listener_ref(&self) -> Result<&net::TcpListener, io::Error> {
        let listener = match self.listener.as_ref() {
            Some(l) => l,
            None => {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    "couldn't get a reference to the listener",
                ))
            }
        };
        Ok(listener)
    }

    pub async fn run(mut self, stopper: impl Future) -> Result<(), io::Error> {
        self.init_listener().await?;

        let listener = self.get_listener_ref().await?;
        let addr = match listener.local_addr() {
            Ok(a) => a.to_string(),
            Err(_) => String::from("[Unknown]"),
        };

        log::info!("Listening on {}", addr);

        let mut rw = self.write_worker.clone();
        tokio::spawn(async move { rw.init_worker().await });

        let mut resw = self.result_worker.clone();
        tokio::spawn(async move { resw.init_worker().await });

        let max_packet_size = self.opts.max_packet_size;
        let read_buffer_size = self.opts.read_buffer_size;
        let no_wait = self.opts.no_wait;
        tokio::select! {
            res = async {
                loop {
                    let (conn, addr) = self.get_listener_ref().await?.accept().await?;
                    log::info!("New connection from: {}", addr);

                    let (rh, wh) = conn.into_split();
                    self.conns.insert(addr.to_string(), wh).await;
                    let sx = self.read_worker.ts.clone();
                    let conns = self.conns.clone();
                    tokio::spawn(async move {
                        let mut handler = RawHandler::new(
                            rh, addr.to_string(), max_packet_size,
                            read_buffer_size, sx, conns
                        );
                        handler.handle();
                    });
                }

                #[allow(unreachable_code)]
                Ok::<_, io::Error>(())
            } => {
                res?;
            }
            _ = stopper => {
                if !no_wait {
                    let _ = self.read_worker.ts.send(Message::Exit).await;
                    log::info!("Sent shutdown signal to workers");
                }
            }
        }

        if !no_wait {
            self.exit.recv().await;
        }
        log::info!("Exited.");
        Ok(())
    }
}
