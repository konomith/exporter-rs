use core::hash::Hash;
use futures::executor::block_on;
use std::collections::HashMap;
use std::ops::{Deref, DerefMut};
use std::sync::Arc;
use tokio::sync::{Mutex, MutexGuard};

#[derive(Debug)]
pub struct MutexInnerConnection<'a, K, V>(MutexGuard<'a, HashMap<K, V>>, K);

impl<'a, K, V> Deref for MutexInnerConnection<'a, K, V>
where
    K: Eq + PartialEq + Hash,
{
    type Target = V;
    fn deref(&self) -> &Self::Target {
        self.0.get(&self.1).unwrap()
    }
}

impl<'a, K, V> DerefMut for MutexInnerConnection<'a, K, V>
where
    K: Eq + PartialEq + Hash,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.0.get_mut(&self.1).unwrap()
    }
}

#[derive(Debug)]
pub struct MutexConnectionContainer<K, V>
where
    K: Eq + PartialEq + Hash,
{
    inner: Arc<Mutex<HashMap<K, V>>>,
}

impl<K, V> MutexConnectionContainer<K, V>
where
    K: Eq + PartialEq + Hash,
{
    pub fn new() -> MutexConnectionContainer<K, V> {
        let map = Arc::new(Mutex::new(HashMap::new()));
        MutexConnectionContainer { inner: map }
    }

    pub async fn insert(&mut self, key: K, value: V) {
        let mut inner = self.inner.lock().await;
        inner.insert(key, value);
    }

    pub async fn get<'a>(&'a self, key: K) -> Option<MutexInnerConnection<'a, K, V>> {
        let inner = self.inner.lock().await;
        if inner.contains_key(&key) {
            return Some(MutexInnerConnection(inner, key));
        }
        None
    }

    pub async fn remove<'a>(&'a mut self, key: K) {
        let mut inner = self.inner.lock().await;
        inner.remove(&key);
    }

    pub fn insert_sync(&mut self, key: K, value: V) {
        block_on(self.insert(key, value))
    }

    pub fn get_sync<'a>(&'a self, key: K) -> Option<MutexInnerConnection<'a, K, V>> {
        block_on(self.get(key))
    }

    pub fn remove_sync<'a>(&'a mut self, key: K) {
        block_on(self.remove(key))
    }
}

impl<K, V> Clone for MutexConnectionContainer<K, V>
where
    K: Eq + PartialEq + Hash,
{
    fn clone(&self) -> Self {
        MutexConnectionContainer {
            inner: self.inner.clone(),
        }
    }
}
