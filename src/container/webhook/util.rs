use std::sync::Arc;

use reqwest::{Client, Error};
use serde::{Deserialize, Serialize};
use serde_json::json;
use serde_json::Value as JsonValue;
use tokio::sync::Mutex;
use tokio::time::{Duration, Instant};

use crate::error::ExporterError;
use crate::server::ServerOptions;
use crate::VERSION;

lazy_static! {
    pub static ref USER_AGENT: String = format!("ExporterServer/{}", VERSION);
}
static TOKEN_ENDPOINT: &str = "realms/{realm}/protocol/openid-connect/token";

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Token {
    access_token: String,
    refresh_token: String,
    expires_in: u64,
    refresh_expires_in: u64,
    token_type: String,
    id_token: String,
    session_state: String,
    not_before_policy: i32,
    scope: String,
}

#[derive(Debug, Clone)]
pub struct TokenManager {
    inner: Arc<Mutex<TokenManagerInner>>,
}

impl TokenManager {
    pub async fn new(opts: Arc<ServerOptions>) -> Result<TokenManager, ExporterError> {
        let inner = TokenManagerInner::new(opts).await?;
        Ok(TokenManager {
            inner: Arc::new(Mutex::new(inner)),
        })
    }

    pub async fn get_token(&self) -> Result<String, ExporterError> {
        let mut inner = self.inner.lock().await;
        let token = inner.get_token().await?;
        Ok(token)
    }
}

async fn get_token(client: &Client, path: &str, payload: JsonValue) -> Result<Token, Error> {
    let res = client
        .post(path)
        .json(&payload)
        .send()
        .await?
        .error_for_status()?;

    let res = res.json::<Token>().await?;
    Ok(res)
}

#[derive(Debug, Clone)]
struct TokenManagerInner {
    client: Client,
    opts: Arc<ServerOptions>,
    token: Token,
    current: Instant,
}

impl TokenManagerInner {
    async fn new(opts: Arc<ServerOptions>) -> Result<TokenManagerInner, ExporterError> {
        let client = Client::builder()
            .user_agent(&*USER_AGENT)
            .connect_timeout(Duration::from_secs(5))
            .build()?;

        let payload = json!({
            "client_id": opts.oauth_client_id.clone(),
            "client_secret": opts.oauth_client_secret.clone(),
            "grant_type": "client_credentials".to_owned(),
        });

        let path = format!(
            "{}/{}",
            opts.oauth_server,
            TOKEN_ENDPOINT.replace("{realm}", &opts.oauth_realm)
        );

        let token = get_token(&client, &path, payload).await?;

        Ok(TokenManagerInner {
            client,
            opts,
            token,
            current: Instant::now(),
        })
    }

    async fn login(&self) -> Result<Token, ExporterError> {
        let payload = json!({
            "client_id": self.opts.oauth_client_id.clone(),
            "client_secret": self.opts.oauth_client_secret.clone(),
            "grant_type": "client_credentials".to_owned(),
        });

        let path = format!(
            "{}/{}",
            self.opts.oauth_server,
            TOKEN_ENDPOINT.replace("{realm}", &self.opts.oauth_realm)
        );

        let token = get_token(&self.client, &path, payload).await?;
        Ok(token)
    }

    async fn refresh_token(&self) -> Result<Token, ExporterError> {
        let payload = json!({
            "client_id": self.opts.oauth_client_id.clone(),
            "refresh_token": self.token.refresh_token.clone(),
            "grant_type": "client_credentials".to_owned(),
        });

        let path = format!(
            "{}/{}",
            self.opts.oauth_server,
            TOKEN_ENDPOINT.replace("{realm}", &self.opts.oauth_realm)
        );

        let token = get_token(&self.client, &path, payload).await?;
        Ok(token)
    }

    async fn get_token(&mut self) -> Result<String, ExporterError> {
        let now = Instant::now();
        let access_token: String;

        if let Some(diff) = now.checked_duration_since(self.current) {
            if diff.as_secs() < self.token.expires_in {
                access_token = self.token.access_token.clone();
            } else if diff.as_secs() < self.token.refresh_expires_in {
                let token = self.refresh_token().await?;
                self.token = token;
                access_token = self.token.access_token.clone();
            } else {
                let token = self.login().await?;
                self.token = token;
                access_token = self.token.access_token.clone();
            }
        } else {
            let token = self.login().await?;
            self.token = token;
            access_token = self.token.access_token.clone();
        }

        self.current = Instant::now();
        Ok(access_token)
    }
}
