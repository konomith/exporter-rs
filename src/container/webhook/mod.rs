use std::collections::HashMap;
use std::convert::TryInto;
use std::sync::Arc;
use std::time::Duration;

use reqwest::{Client, StatusCode};

use crate::error::ExporterError;
use crate::server::ServerOptions;

mod util;

#[derive(Debug, Clone)]
pub struct WebhookClient {
    client: Client,
    manager: Option<util::TokenManager>,
}

impl WebhookClient {
    pub async fn new(opts: Arc<ServerOptions>) -> Result<WebhookClient, ExporterError> {
        let manager = match util::TokenManager::new(opts.clone()).await {
            Ok(new) => Some(new),
            Err(e) => {
                log::warn!("Failed to create token manager: {}", e);
                None
            }
        };

        let client = Client::builder()
            .user_agent(&*util::USER_AGENT)
            .connect_timeout(Duration::from_millis(opts.webhook_connect_timeout))
            .pool_max_idle_per_host(20)
            .pool_idle_timeout(Duration::from_secs(30))
            .danger_accept_invalid_certs(opts.webhook_skip_cert_verify)
            .build()?;
        Ok(WebhookClient { client, manager })
    }

    pub async fn send(
        &self,
        address: &str,
        payload: &String,
        headers: &HashMap<String, String>,
        auth_required: bool,
    ) -> Result<StatusCode, ExporterError> {
        let mut req = self
            .client
            .post(address)
            .headers(headers.try_into()?)
            .body((*payload).clone());

        if auth_required {
            if self.manager.is_none() {
                return Err(ExporterError::NoTokenManager);
            }

            let token = self.manager.as_ref().unwrap().get_token().await?;
            let value = format!("Bearer {}", token);
            req = req.header("Authorization", &value);
        }

        let resp = req.send().await?;
        Ok(resp.status())
    }
}
