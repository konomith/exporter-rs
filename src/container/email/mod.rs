use std::sync::Arc;

use lettre::{
    transport::smtp::authentication::Credentials, AsyncSmtpTransport, AsyncTransport, Message,
    Tokio1Executor,
};
use log;
use tokio::sync::Mutex;

use crate::error::ExporterError;
use crate::server::EmailOptions;

mod util;

#[derive(Clone)]
struct EmailClientInner {
    transport: Option<AsyncSmtpTransport<Tokio1Executor>>,
    options: Arc<EmailOptions>,
    retried: bool,
}

impl EmailClientInner {
    fn new(options: Arc<EmailOptions>) -> EmailClientInner {
        EmailClientInner {
            transport: None,
            options,
            retried: false,
        }
    }

    async fn connect(&mut self) -> Result<(), ExporterError> {
        let mut builder = if self.options.tls && !self.options.skip_tls_verify {
            log::debug!("Using ::relay to connect to {}", self.options.server);
            AsyncSmtpTransport::<Tokio1Executor>::relay(&self.options.server)?
        } else {
            log::debug!(
                "Using ::builder_dangerous to connect to {}",
                self.options.server
            );
            AsyncSmtpTransport::<Tokio1Executor>::builder_dangerous(&self.options.server)
        };
        builder = builder.port(self.options.port);

        if self.options.auth {
            let creds =
                Credentials::new(self.options.username.clone(), self.options.password.clone());
            builder = builder.credentials(creds);
        }

        let mailer: AsyncSmtpTransport<Tokio1Executor> = builder.build();
        self.transport = Some(mailer);

        Ok(())
    }

    async fn send(&mut self, message: Message) -> Result<(), ExporterError> {
        if self.transport.is_none() {
            self.connect().await?;
        }

        if let Some(ref mailer) = self.transport {
            log::debug!("Initiating send... {:?}", message);

            match mailer.send(message).await {
                Ok(result) => {
                    log::debug!("mailer response: {:?}", result);
                    self.retried = false;

                    if !result.is_positive() {
                        let code = result.code().to_string();
                        return Err(ExporterError::SMTPServerError(code));
                    }
                    return Ok(());
                }
                Err(err) => {
                    log::debug!("mailer err: {}|retired={}", err, self.retried);
                    if !self.retried {
                        self.retried = true;
                        self.transport = None;
                        return Err(ExporterError::Retry);
                    } else {
                        return Err(ExporterError::from(err));
                    }
                }
            }
        }

        Ok(())
    }
}

#[derive(Clone)]
pub struct EmailClient {
    inner: Arc<Mutex<EmailClientInner>>,
}

impl EmailClient {
    pub fn new(options: Arc<EmailOptions>) -> EmailClient {
        let inner = EmailClientInner::new(options);
        EmailClient {
            inner: Arc::new(Mutex::new(inner)),
        }
    }

    pub async fn send(
        &self,
        to: &str,
        subject: &str,
        body: &str,
        attachments: &Vec<String>,
        attachments_filenames: &Vec<String>,
    ) -> Result<(), ExporterError> {
        let mut inner = self.inner.lock().await;
        let from = inner.options.username.clone();

        let message =
            util::prepare_email(&from, to, subject, body, attachments, attachments_filenames)?;

        match inner.send(message).await {
            Ok(_) => Ok(()),
            Err(err) => match err {
                ExporterError::Retry => {
                    let message = util::prepare_email(
                        &from,
                        to,
                        subject,
                        body,
                        attachments,
                        attachments_filenames,
                    )?;
                    let _ = inner.send(message).await?;
                    Ok(())
                }
                _ => Err(err),
            },
        }
    }
}
