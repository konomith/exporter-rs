use std::io::prelude::*;

use base64;
use flate2::read::GzDecoder;
use lettre::message::{header, Mailbox, Message, MultiPart, SinglePart};
use log;

use crate::error::ExporterError;

pub fn decode_attachment_content(content: &str) -> Result<Vec<u8>, ExporterError> {
    let decoded = base64::decode(content)?;
    log::debug!("[decode_attachment_content]: b64decode: {:?}", decoded);

    let mut gz_decoder = GzDecoder::new(&decoded[..]);
    let mut buf = Vec::new();
    let read = gz_decoder.read_to_end(&mut buf)?;

    log::debug!(
        "[decode_attachment_content]: read: {}, buf: {:?}",
        read,
        buf
    );

    Ok(buf)
}

pub fn prepare_email(
    from: &str,
    to: &str,
    subject: &str,
    body: &str,
    attachments: &Vec<String>,
    attachments_filenames: &Vec<String>,
) -> Result<Message, ExporterError> {
    let from = parse_to_mailbox(&from)?;
    let to = parse_to_mailbox(&to)?;

    let message = Message::builder()
        .from(from)
        .to(to)
        .subject(subject)
        .multipart(prepare_mixed_multipart(
            body,
            attachments,
            attachments_filenames,
        )?)?;

    Ok(message)
}

pub fn prepare_attachment(filename: &str, content: &str) -> Result<SinglePart, ExporterError> {
    let content = decode_attachment_content(content)?;

    let attachment = SinglePart::builder()
        .header(header::ContentType::TEXT_PLAIN)
        .header(header::ContentDisposition::attachment(filename))
        .body(content);
    Ok(attachment)
}

pub fn prepare_mixed_multipart(
    body: &str,
    attachments: &Vec<String>,
    attachments_filenames: &Vec<String>,
) -> Result<MultiPart, ExporterError> {
    let mut mp = MultiPart::mixed().singlepart(prepare_body(body.as_bytes().to_vec()));

    let attachment_iter = attachments_filenames.iter().zip(attachments.iter());

    for (fname, content) in attachment_iter {
        let attachment = prepare_attachment(&fname, &content)?;
        mp = mp.singlepart(attachment);
    }

    Ok(mp)
}

pub fn prepare_body(content: Vec<u8>) -> SinglePart {
    SinglePart::builder()
        .header(header::ContentType::TEXT_PLAIN)
        .body(content)
}

pub fn parse_to_mailbox(addr: &str) -> Result<Mailbox, ExporterError> {
    let m: Mailbox = addr.parse()?;
    Ok(m)
}
