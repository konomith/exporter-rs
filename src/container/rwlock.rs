use core::hash::Hash;
use futures::executor::block_on;
use std::collections::HashMap;
use std::ops::Deref;
use std::sync::Arc;
use tokio::sync::{RwLock, RwLockReadGuard};

#[derive(Debug)]
pub struct RwLockInnerConnection<'a, K, V>(RwLockReadGuard<'a, HashMap<K, V>>, K);

impl<'a, K, V> Deref for RwLockInnerConnection<'a, K, V>
where
    K: Eq + PartialEq + Hash,
{
    type Target = V;
    fn deref(&self) -> &Self::Target {
        self.0.get(&self.1).unwrap()
    }
}

#[derive(Debug)]
pub struct RwLockConnectionContainer<K, V>
where
    K: Eq + PartialEq + Hash,
{
    inner: Arc<RwLock<HashMap<K, V>>>,
}

impl<K, V> RwLockConnectionContainer<K, V>
where
    K: Eq + PartialEq + Hash,
{
    pub fn new() -> RwLockConnectionContainer<K, V> {
        let map = Arc::new(RwLock::new(HashMap::new()));
        RwLockConnectionContainer { inner: map }
    }

    pub async fn insert(&mut self, key: K, value: V) {
        let mut inner = self.inner.write().await;
        inner.insert(key, value);
    }

    pub async fn get<'a>(&'a self, key: K) -> Option<RwLockInnerConnection<'a, K, V>> {
        let inner = self.inner.read().await;
        if inner.contains_key(&key) {
            return Some(RwLockInnerConnection(inner, key));
        }
        None
    }

    pub async fn remove<'a>(&'a mut self, key: K) {
        let mut inner = self.inner.write().await;
        inner.remove(&key);
    }

    pub fn insert_sync(&mut self, key: K, value: V) {
        block_on(self.insert(key, value))
    }

    pub fn get_sync<'a>(&'a self, key: K) -> Option<RwLockInnerConnection<'a, K, V>> {
        block_on(self.get(key))
    }

    pub fn remove_sync<'a>(&'a mut self, key: K) {
        block_on(self.remove(key))
    }
}

impl<K, V> Clone for RwLockConnectionContainer<K, V>
where
    K: Eq + PartialEq + Hash,
{
    fn clone(&self) -> Self {
        RwLockConnectionContainer {
            inner: self.inner.clone(),
        }
    }
}
