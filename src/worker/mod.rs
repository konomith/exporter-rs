use std::sync::{Arc, RwLock};
use std::time::{Duration, Instant};

use async_std::channel::{bounded, Receiver as AsyncReceiver, Sender as AsyncSender};
use bb8::Pool as ConnectionPool;
use log;
use tokio::io::AsyncWriteExt;
use tokio::net;
use tokio::sync::mpsc;

use crate::container::{
    EmailClient, MutexConnectionContainer, RwLockConnectionContainer, WebhookClient,
};
use crate::error::ExporterError;
use crate::handler::*;
use crate::message::*;
use crate::packet::*;
use crate::pool::{get_udp_pool, TcpConnectionManager, UdpConnectionManager};
use crate::server::ServerOptions;

macro_rules! TASK_INFO_FMT {
    () => {
        "{} Finish time={:?} queued={:?} size={} packet_id={} client={} dest={} result={:?} (error={})"
    };
}

fn display_result(
    task: &Task,
    result: &TaskResult,
    packet_type: &str,
    packet_id: u64,
    dst: String,
    queued: Duration,
    duration: Duration,
) {
    log::info!(
        TASK_INFO_FMT!(),
        packet_type,
        duration,
        queued,
        task.payload.len(),
        packet_id,
        task.client_id,
        dst,
        result.result,
        result
            .error_detail
            .as_ref()
            .unwrap_or(&String::from("None"))
    );
}

struct WorkerHandler;

impl WorkerHandler {
    async fn handle_tcp(
        task: &Task,
        opts: Arc<ServerOptions>,
        pools: RwLockConnectionContainer<String, ConnectionPool<TcpConnectionManager>>,
    ) -> TaskResult {
        let result;
        let parsed = parse_protocol_packet(&task.payload);
        match parsed {
            Ok(packet) => {
                let queued = task.created.elapsed();

                let handler = TcpHandler::new(&packet, opts, pools, task.client_id.clone());

                let start = Instant::now();
                result = handler.handle().await;
                let end = start.elapsed();

                let dst = format!("{}:{}", packet.addr, packet.port);
                display_result(&task, &result, "TCP", packet.id, dst, queued, end);
            }
            Err(e) => {
                log::warn!("Couldn't parse tcp packet: {}", e);
                result = TaskResult {
                    client_id: task.client_id.to_owned(),
                    result: "-1|0x01\r\n".to_string(),
                    error_detail: Some("Couldn't parse tcp packet".to_string()),
                };
            }
        }

        result
    }

    async fn handle_webhook(
        task: &Task,
        client: &WebhookClient,
    ) -> TaskResult {
        let result;

        let parsed = parse_webhook_packet(&task.payload);
        match parsed {
            Ok(packet) => {
                let queued = task.created.elapsed();

                let handler =
                    WebhookHandler::new(&packet, client.clone(), task.client_id.clone());

                let start = Instant::now();
                result = handler.handle().await;
                let end = start.elapsed();

                display_result(
                    &task,
                    &result,
                    "WEBHOOK",
                    packet.id,
                    packet.address.to_owned(),
                    queued,
                    end,
                );
            }
            Err(e) => {
                log::warn!("Couldn't parse webhook packet: {}", e);
                result = TaskResult {
                    client_id: task.client_id.to_owned(),
                    result: "-1|0x01\r\n".to_string(),
                    error_detail: Some("Couldn't parse webhook packet".to_string()),
                };
            }
        }
        result
    }

    async fn handle_email(task: &Task, client: &EmailClient) -> TaskResult {
        let result;

        let parsed = parse_email_packet(&task.payload);
        match parsed {
            Ok(packet) => {
                let queued = task.created.elapsed();

                let handler = EmailHandler::new(&packet, client.clone(), task.client_id.clone());

                let start = Instant::now();
                result = handler.handle().await;
                let end = start.elapsed();

                display_result(
                    &task,
                    &result,
                    "EMAIL",
                    packet.id,
                    packet.email.to_owned(),
                    queued,
                    end,
                );
            }
            Err(e) => {
                log::warn!("Couldn't parse email packet: {}", e);
                result = TaskResult {
                    client_id: task.client_id.to_owned(),
                    result: "-1|0x01\r\n".to_string(),
                    error_detail: Some("Couldn't parse email packet".to_string()),
                };
            }
        }
        result
    }

    async fn handle_udp(
        task: &Task,
        pool: ConnectionPool<UdpConnectionManager>,
    ) -> TaskResult {
        let result;

        let parsed = parse_protocol_packet(&task.payload);
        match parsed {
            Ok(packet) => {
                let queued = task.created.elapsed();
                let handler = UdpHandler::new(&packet, pool, task.client_id.clone());
                let start = Instant::now();
                result = handler.handle().await;
                let end = start.elapsed();

                let dst = format!("{}:{}", packet.addr, packet.port);
                display_result(&task, &result, "UDP", packet.id, dst, queued, end);
            }
            Err(e) => {
                log::warn!("Couldn't parse udp packet: {}", e);
                result = TaskResult {
                    client_id: task.client_id.to_owned(),
                    result: "-1|0x01\r\n".to_string(),
                    error_detail: Some("Couldn't parse udp packet".to_string()),
                };
            }
        }
        result
    }
}

#[derive(Debug, Clone)]
struct TcpWorker {
    receiver: AsyncReceiver<Message>,
    sender: mpsc::Sender<Message>,
    opts: Arc<ServerOptions>,
    tcp_pools: RwLockConnectionContainer<String, ConnectionPool<TcpConnectionManager>>,
}

impl TcpWorker {
    fn new(
        receiver: AsyncReceiver<Message>,
        sender: mpsc::Sender<Message>,
        opts: Arc<ServerOptions>,
        tcp_pools: RwLockConnectionContainer<String, ConnectionPool<TcpConnectionManager>>,
    ) -> TcpWorker {
        TcpWorker {
            receiver,
            sender,
            opts,
            tcp_pools,
        }
    }

    async fn init_worker(&self) {
        let max_workers = self.opts.tcp_workers;

        for _ in 0..max_workers {
            let receiver = self.receiver.clone();
            let sender = self.sender.clone();
            let pools = self.tcp_pools.clone();
            let opts = self.opts.clone();

            tokio::spawn(async move {
                let receiver = receiver.clone();
                let sender = sender.clone();

                loop {
                    match receiver.recv().await {
                        Ok(msg) => match msg {
                            Message::Exit => {
                                break;
                            }
                            Message::T(task) => {
                                let result =
                                    WorkerHandler::handle_tcp(&task, opts.clone(), pools.clone())
                                        .await;
                                let _ = sender.send(Message::TR(result)).await;
                            }
                            _ => {}
                        },
                        Err(e) => {
                            log::trace!("TcpWorker: init_worker err in loop: {}", e);
                            break;
                        }
                    }
                }
            });
        }
    }
}

#[derive(Debug, Clone)]
pub(crate) struct WebhookWorker {
    receiver: AsyncReceiver<Message>,
    sender: mpsc::Sender<Message>,
    opts: Arc<ServerOptions>,
    client: WebhookClient,
}

impl WebhookWorker {
    pub(crate) async fn new(
        receiver: AsyncReceiver<Message>,
        sender: mpsc::Sender<Message>,
        opts: Arc<ServerOptions>,
    ) -> Result<WebhookWorker, ExporterError> {
        let client = WebhookClient::new(opts.clone()).await?;
        Ok(WebhookWorker {
            receiver,
            sender,
            opts,
            client,
        })
    }

    pub(crate) async fn init_worker(&self) {
        let max_workers = self.opts.webhook_workers;

        for _ in 0..max_workers {
            let receiver = self.receiver.clone();
            let sender = self.sender.clone();
            let client = self.client.clone();

            tokio::spawn(async move {
                let receiver = receiver.clone();
                let sender = sender.clone();

                loop {
                    match receiver.recv().await {
                        Ok(msg) => match msg {
                            Message::Exit => {
                                break;
                            }
                            Message::T(task) => {
                                let result =
                                    WorkerHandler::handle_webhook(&task, &client)
                                        .await;
                                let _ = sender.send(Message::TR(result)).await;
                            }
                            _ => {}
                        },
                        Err(e) => {
                            log::trace!("WebhookWorker: init_worker err in loop: {}", e);
                            break;
                        }
                    }
                }
            });
        }
    }
}

#[derive(Clone)]
pub(crate) struct EmailWorker {
    receiver: AsyncReceiver<Message>,
    sender: mpsc::Sender<Message>,
    opts: Arc<ServerOptions>,
    client: EmailClient,
}

impl EmailWorker {
    pub(crate) async fn new(
        receiver: AsyncReceiver<Message>,
        sender: mpsc::Sender<Message>,
        opts: Arc<ServerOptions>,
    ) -> Result<EmailWorker, ExporterError> {
        let email_options = Arc::new(opts.email_options.clone());
        let client = EmailClient::new(email_options.clone());
        Ok(EmailWorker {
            receiver,
            sender,
            opts,
            client,
        })
    }

    pub(crate) async fn init_worker(&self) {
        let max_workers = self.opts.email_workers;

        for _ in 0..max_workers {
            let receiver = self.receiver.clone();
            let sender = self.sender.clone();
            let client = self.client.clone();

            tokio::spawn(async move {
                let receiver = receiver.clone();
                let sender = sender.clone();

                loop {
                    match receiver.recv().await {
                        Ok(msg) => match msg {
                            Message::Exit => {
                                break;
                            }
                            Message::T(task) => {
                                let result = WorkerHandler::handle_email(&task, &client).await;
                                let _ = sender.send(Message::TR(result)).await;
                            }
                            _ => {}
                        },
                        Err(e) => {
                            log::trace!("EmailWorker: init_worker err in loop: {}", e);
                            break;
                        }
                    }
                }
            });
        }
    }
}

#[derive(Debug, Clone)]
struct UdpWorker {
    receiver: AsyncReceiver<Message>,
    sender: mpsc::Sender<Message>,
    opts: Arc<ServerOptions>,
    pool: ConnectionPool<UdpConnectionManager>,
}

impl UdpWorker {
    async fn new(
        receiver: AsyncReceiver<Message>,
        sender: mpsc::Sender<Message>,
        opts: Arc<ServerOptions>,
    ) -> UdpWorker {
        let pool = get_udp_pool(
            opts.udp_pool_size,
            Duration::from_millis(opts.udp_idle_timeout),
        )
        .await
        .expect("Couldn't create UDP pool");
        UdpWorker {
            receiver,
            sender,
            opts,
            pool,
        }
    }

    async fn init_worker(&self) {
        let max_workers = self.opts.udp_workers;

        for _ in 0..max_workers {
            let receiver = self.receiver.clone();
            let sender = self.sender.clone();
            let pool = self.pool.clone();

            tokio::spawn(async move {
                let receiver = receiver.clone();
                let sender = sender.clone();

                loop {
                    match receiver.recv().await {
                        Ok(msg) => match msg {
                            Message::Exit => {
                                break;
                            }
                            Message::T(task) => {
                                let result =
                                    WorkerHandler::handle_udp(&task, pool.clone())
                                        .await;
                                let _ = sender.send(Message::TR(result)).await;
                            }
                            _ => {}
                        },
                        Err(e) => {
                            log::trace!("UdpWorker: init_worker err in loop: {}", e);
                            break;
                        }
                    }
                }
            });
        }
    }
}

#[derive(Debug, Clone)]
pub(crate) struct WriteWorker {
    task_sender: mpsc::Sender<Message>,
    task_receiver: Arc<RwLock<Option<mpsc::Receiver<Message>>>>,
    tcp_pools: RwLockConnectionContainer<String, ConnectionPool<TcpConnectionManager>>,
    opts: Arc<ServerOptions>,
}

impl WriteWorker {
    pub(crate) fn new(
        task_sender: mpsc::Sender<Message>,
        task_receiver: Arc<RwLock<Option<mpsc::Receiver<Message>>>>,
        opts: Arc<ServerOptions>,
    ) -> WriteWorker {
        let tcp_pools =
            RwLockConnectionContainer::<String, ConnectionPool<TcpConnectionManager>>::new();
        WriteWorker {
            task_sender,
            task_receiver,
            tcp_pools,
            opts,
        }
    }

    async fn init_tcp_worker(&mut self) -> AsyncSender<Message> {
        let ts = self.task_sender.clone();
        let pools = self.tcp_pools.clone();

        let (tcp_sender, tcp_receiver) = bounded::<Message>(self.opts.task_buff_size);
        let tcp_worker = TcpWorker::new(tcp_receiver, ts.clone(), self.opts.clone(), pools);

        tokio::spawn(async move {
            tcp_worker.init_worker().await;
        });

        tcp_sender
    }

    async fn init_webhook_worker(&mut self) -> AsyncSender<Message> {
        let ts = self.task_sender.clone();
        let (webhook_sender, webhook_receiver) = bounded::<Message>(self.opts.task_buff_size);

        let webhook_worker = WebhookWorker::new(webhook_receiver, ts.clone(), self.opts.clone())
            .await
            .expect("Couldn't start internal webhook workers");

        tokio::spawn(async move {
            webhook_worker.init_worker().await;
        });

        webhook_sender
    }

    async fn init_udp_worker(&mut self) -> AsyncSender<Message> {
        let ts = self.task_sender.clone();
        let (udp_sender, udp_receiver) = bounded::<Message>(self.opts.task_buff_size);
        let udp_worker = UdpWorker::new(udp_receiver, ts.clone(), self.opts.clone()).await;

        tokio::spawn(async move {
            udp_worker.init_worker().await;
        });

        udp_sender
    }

    async fn init_email_worker(&mut self) -> AsyncSender<Message> {
        let ts = self.task_sender.clone();
        let (email_sender, email_receiver) = bounded::<Message>(self.opts.task_buff_size);

        let email_worker = EmailWorker::new(email_receiver, ts.clone(), self.opts.clone())
            .await
            .expect("Couldn't start internal webhook workers");

        tokio::spawn(async move {
            email_worker.init_worker().await;
        });

        email_sender
    }

    pub(crate) async fn init_worker(&mut self) {
        let mut receiver = self.task_receiver.write().unwrap().take().unwrap();

        let ts = self.task_sender.clone();

        let tcp_sender = self.init_tcp_worker().await;
        let webhook_sender = self.init_webhook_worker().await;
        let udp_sender = self.init_udp_worker().await;
        let email_sender = self.init_email_worker().await;

        tokio::spawn(async move {
            while let Some(i) = receiver.recv().await {
                match i {
                    Message::T(task) => match parse_packet(&task.payload) {
                        PacketType::TCP => match tcp_sender.send(Message::T(task)).await {
                            Ok(_) => {}
                            Err(e) => {
                                log::warn!("Failed to send to tcp worker: {}", e);
                            }
                        },
                        PacketType::WEBHOOK => match webhook_sender.send(Message::T(task)).await {
                            Ok(_) => {}
                            Err(e) => {
                                log::warn!("Failed to send to webhook worker: {}", e);
                            }
                        },
                        PacketType::UDP => match udp_sender.send(Message::T(task)).await {
                            Ok(_) => {}
                            Err(e) => {
                                log::warn!("Failed to send to udp worker: {}", e);
                            }
                        },
                        PacketType::EMAIL => match email_sender.send(Message::T(task)).await {
                            Ok(_) => {}
                            Err(e) => {
                                log::warn!("Failed to send to email worker: {}", e);
                            }
                        },
                        _ => {
                            let tr = TaskResult {
                                client_id: task.client_id.clone(),
                                result: "-1|0x02\r\n".to_string(),
                                error_detail: Some(String::from("Invalid PacketType")),
                            };
                            let _ = ts.send(Message::TR(tr)).await;
                            continue;
                        }
                    },
                    Message::TR(task) => {
                        log::info!("WriteWorker got TR => {:?}", task);
                        let _ = ts.send(Message::TR(task)).await;
                    }
                    Message::Exit => {
                        let _ = tcp_sender.send(Message::Exit).await;
                        let _ = ts.send(Message::Exit).await;
                        log::info!("WriteWorker: exit");
                        break;
                    }
                    Message::E(err) => {
                        log::warn!("Error received from RawHandler: {}", err);
                    }
                }
            }
        });
    }
}

#[derive(Debug)]
pub(crate) struct ReadWorker {
    pub(crate) ts: mpsc::Sender<Message>,
}

#[derive(Debug, Clone)]
pub(crate) struct ResultWorker {
    task_receiver: Arc<RwLock<Option<mpsc::Receiver<Message>>>>,
    conns: MutexConnectionContainer<String, net::tcp::OwnedWriteHalf>,
    exit_sender: mpsc::Sender<()>,
}

impl ResultWorker {
    pub(crate) fn new(
        task_receiver: Arc<RwLock<Option<mpsc::Receiver<Message>>>>,
        conns: MutexConnectionContainer<String, net::tcp::OwnedWriteHalf>,
        exit_sender: mpsc::Sender<()>,
    ) -> ResultWorker {
        ResultWorker {
            task_receiver,
            conns,
            exit_sender,
        }
    }

    pub(crate) async fn init_worker(&mut self) {
        let mut receiver = self.task_receiver.write().unwrap().take().unwrap();
        let exit = self.exit_sender.clone();

        let conns = self.conns.clone();
        tokio::spawn(async move {
            while let Some(i) = receiver.recv().await {
                match i {
                    Message::TR(task) => {
                        match conns.get(task.client_id.clone()).await {
                            Some(mut conn) => match conn.write_all(task.result.as_bytes()).await {
                                Ok(_) => {}
                                Err(e) => {
                                    log::warn!(
                                        "Error while replying to '{}': {}",
                                        task.client_id,
                                        e
                                    );
                                    conns.clone().remove(task.client_id.clone()).await;
                                }
                            },
                            None => {
                                log::warn!("Client '{}' has disconnected", task.client_id);
                            }
                        };
                    }
                    Message::Exit => {
                        log::info!("ResultWorker: exit");
                        let _ = exit.send(()).await;
                        break;
                    }
                    _ => {
                        log::debug!("ResultWorker: bad message type");
                    }
                }
            }
        });
    }
}
