use std::time::Instant;

use super::error::ExporterError;

#[derive(Debug)]
pub struct Task {
    pub client_id: String,
    pub payload: String,
    pub created: Instant,
}

impl Task {
    pub fn new(client_id: String, payload: String) -> Task {
        Task {
            client_id,
            payload,
            created: Instant::now(),
        }
    }
}

#[derive(Debug)]
pub struct TaskResult {
    pub client_id: String,
    pub result: String,
    pub error_detail: Option<String>,
}

#[derive(Debug)]
pub enum Message {
    T(Task),
    TR(TaskResult),
    E(ExporterError),
    Exit,
}
