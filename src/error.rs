use std::io::Error as StdIoError;

use base64;
use err_derive;
use http::Error as HttpGenericError;
use lettre::{
    address::AddressError as LettreAddressError, error::Error as LettreError,
    transport::smtp::Error as LettreSmtpError,
};
use reqwest::Error as ReqwestError;

use crate::parser::CodecError;

#[derive(err_derive::Error, Debug)]
pub enum ExporterError {
    #[error(display = "Max packet size exceeded")]
    ParserError(#[source] CodecError),

    #[error(display = "Webhook Error: {}", _0)]
    WebhookError(#[source] ReqwestError),

    #[error(display = "HTTP Error: {}", _0)]
    HttpError(#[source] HttpGenericError),

    #[error(display = "No token manager available")]
    NoTokenManager,

    #[error(display = "Couldn't decode attachment: {}", _0)]
    AttachmentDecodeError(#[source] base64::DecodeError),

    #[error(display = "IOError: {}", _0)]
    IOError(#[source] StdIoError),

    #[error(display = "EmailAddressError: {}", _0)]
    EmailAddressError(#[source] LettreAddressError),

    #[error(display = "EmailError: {}", _0)]
    EmailError(#[source] LettreError),

    #[error(display = "SMPTError: {}", _0)]
    SMPTError(#[source] LettreSmtpError),

    #[error(display = "SMTPServerError: Server replied with: {}", _0)]
    SMTPServerError(String),

    #[error(display = "Retry")]
    Retry,
}
