use bytes::{Buf, BufMut, BytesMut};
use std::{cmp, fmt, io, str, usize};

use tokio_util::codec::{Decoder, Encoder};

#[derive(Debug)]
pub enum CodecError {
    MaxPacketSizeExceeded,
    Io(io::Error),
}

impl fmt::Display for CodecError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            CodecError::MaxPacketSizeExceeded => write!(f, "max packet size exceeded"),
            CodecError::Io(e) => write!(f, "{}", e),
        }
    }
}

impl From<io::Error> for CodecError {
    fn from(e: io::Error) -> CodecError {
        CodecError::Io(e)
    }
}

impl std::error::Error for CodecError {}

// implementation derived from LinesCodec
#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct NullByteCodec {
    next_index: usize,
    max_packet_size: usize,
    is_discarding: bool,
}

fn utf8(buf: &[u8]) -> Result<&str, io::Error> {
    str::from_utf8(buf)
        .map_err(|_| io::Error::new(io::ErrorKind::InvalidData, "Unable to decode input as UTF8"))
}

impl NullByteCodec {
    pub fn new() -> NullByteCodec {
        NullByteCodec {
            next_index: 0,
            max_packet_size: usize::MAX,
            is_discarding: false,
        }
    }

    pub fn new_with_max_packet_size(max_packet_size: usize) -> Self {
        NullByteCodec {
            max_packet_size,
            ..NullByteCodec::new()
        }
    }

    #[allow(dead_code)]
    pub fn max_packet_size(&self) -> usize {
        self.max_packet_size
    }
}

impl Decoder for NullByteCodec {
    type Item = String;
    type Error = CodecError;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        loop {
            let read_to = cmp::min(self.max_packet_size.saturating_add(1), src.len());

            let null_offset = src[self.next_index..read_to]
                .iter()
                .position(|b| *b == b'\0');

            match (self.is_discarding, null_offset) {
                (true, Some(offset)) => {
                    src.advance(offset + self.next_index + 1);
                    self.is_discarding = false;
                    self.next_index = 0;
                }
                (true, None) => {
                    src.advance(read_to);
                    self.next_index = 0;
                    if src.is_empty() {
                        return Err(CodecError::MaxPacketSizeExceeded);
                    }
                }
                (false, Some(offset)) => {
                    let null_index = offset + self.next_index;
                    self.next_index = 0;
                    let packet = src.split_to(null_index + 1);
                    let packet = &packet[..packet.len() - 1];
                    let packet = utf8(packet)?;
                    return Ok(Some(packet.to_string()));
                }
                (false, None) if src.len() > self.max_packet_size => {
                    self.is_discarding = true;
                    return Err(CodecError::MaxPacketSizeExceeded);
                }
                (false, None) => {
                    self.next_index = read_to;
                    return Ok(None);
                }
            }
        }
    }
}

impl<T> Encoder<T> for NullByteCodec
where
    T: AsRef<str>,
{
    type Error = CodecError;

    fn encode(&mut self, line: T, buf: &mut BytesMut) -> Result<(), CodecError> {
        let line = line.as_ref();
        buf.reserve(line.len() + 1);
        buf.put(line.as_bytes());
        buf.put_u8(b'\0');
        Ok(())
    }
}

impl Default for NullByteCodec {
    fn default() -> Self {
        Self::new()
    }
}
