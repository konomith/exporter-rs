use log;

use tokio::net::tcp;
use tokio::sync::mpsc;
use tokio_stream::StreamExt;
use tokio_util::codec::FramedRead;

use crate::container::MutexConnectionContainer;
use crate::error::ExporterError;
use crate::message::{Message, Task};
use crate::parser::NullByteCodec;

#[derive(Debug)]
pub struct RawHandler {
    frame: Option<FramedRead<tcp::OwnedReadHalf, NullByteCodec>>,
    client_id: String,
    sender: mpsc::Sender<Message>,
    conns: MutexConnectionContainer<String, tcp::OwnedWriteHalf>,
}

impl RawHandler {
    pub fn new(
        client: tcp::OwnedReadHalf,
        client_id: String,
        packet_size: usize,
        buffer_size: usize,
        sender: mpsc::Sender<Message>,
        conns: MutexConnectionContainer<String, tcp::OwnedWriteHalf>,
    ) -> RawHandler {
        let frame = FramedRead::with_capacity(
            client,
            NullByteCodec::new_with_max_packet_size(packet_size),
            buffer_size,
        );
        RawHandler {
            frame: Some(frame),
            client_id,
            sender,
            conns,
        }
    }

    pub fn handle(&mut self) {
        let mut frame = self.frame.take().unwrap();
        let cid = self.client_id.clone();
        let tx = self.sender.clone();
        let mut conns = self.conns.clone();

        tokio::spawn(async move {
            while let Some(packet) = frame.next().await {
                match packet {
                    Ok(data) => {
                        let task = Task::new(cid.clone(), data);
                        let message = Message::T(task);
                        match tx.send(message).await {
                            Err(e) => {
                                log::warn!("RawHandler failed to send task: {:?}", e);
                                break;
                            }
                            Ok(_) => {}
                        };
                    }
                    Err(err) => {
                        let message = Message::E(ExporterError::from(err));
                        match tx.send(message).await {
                            Err(e) => {
                                log::warn!("RawHandler failed to send error: {:?}", e);
                                break;
                            }
                            Ok(_) => {}
                        };
                        break;
                    }
                }
            }
            conns.remove(cid.clone()).await;
            log::info!("Client {} exited.", cid);
        });
    }
}
