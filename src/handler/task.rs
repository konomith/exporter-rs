use std::sync::Arc;
use std::time::Duration;

use bb8::Pool as ConnectionPool;
use log;
use tokio::io::AsyncWriteExt;

use crate::container::{
    EmailClient, RwLockConnectionContainer, RwLockInnerConnection, WebhookClient,
};
use crate::error::ExporterError;
use crate::message::TaskResult;
use crate::packet;
use crate::pool::{get_tcp_pool, TcpConnectionManager, UdpConnectionManager};
use crate::server::ServerOptions;

#[derive(Debug)]
pub struct TcpHandler<'a> {
    packet: &'a packet::ProtocolPacket,
    options: Arc<ServerOptions>,
    pools: RwLockConnectionContainer<String, ConnectionPool<TcpConnectionManager>>,
    client_id: String,
}

impl<'a> TcpHandler<'a> {
    pub fn new(
        packet: &packet::ProtocolPacket,
        options: Arc<ServerOptions>,
        pools: RwLockConnectionContainer<String, ConnectionPool<TcpConnectionManager>>,
        client_id: String,
    ) -> TcpHandler {
        TcpHandler {
            packet,
            options,
            pools,
            client_id,
        }
    }

    pub async fn handle(&self) -> TaskResult {
        let mut res = String::new();
        let mut err = None;

        let destination = format!("{}:{}", self.packet.addr, self.packet.port);
        let pool = match self.get_pool(&destination).await {
            Some(p) => p,
            None => {
                log::warn!("Couldn't get pool for: {}", destination);
                err = Some(format!("No connection pool for {}", destination));
                res.push_str(&format!("{}|0x01\r\n", self.packet.id));

                return TaskResult {
                    client_id: self.client_id.clone(),
                    result: res.clone(),
                    error_detail: err,
                };
            }
        };

        let mut client = match pool.get().await {
            Ok(c) => c,
            Err(e) => {
                log::warn!("Couldn't get client for {}: {}", destination, e);
                err = Some(format!("No client: {}", e));
                res.push_str(&format!("{}|0x02\r\n", self.packet.id));

                return TaskResult {
                    client_id: self.client_id.clone(),
                    result: res.clone(),
                    error_detail: err,
                };
            }
        };

        let write_res = client.write_all(self.packet.payload.as_bytes()).await;
        if write_res.is_ok() {
            res.push_str(&format!("{}|0x00\r\n", self.packet.id));
        } else {
            let _ = client.shutdown();

            log::warn!(
                "TCP: Couldn't write to destination {}: {:?}",
                destination,
                write_res
            );
            err = Some(format!("Write failed: {:#?}", write_res.err()));

            res.push_str(&format!("{}|0x02\r\n", self.packet.id))
        }

        TaskResult {
            client_id: self.client_id.clone(),
            result: res.clone(),
            error_detail: err,
        }
    }

    async fn get_pool(
        &self,
        addr: &str,
    ) -> Option<RwLockInnerConnection<'_, String, ConnectionPool<TcpConnectionManager>>> {
        match self.pools.get(addr.to_string()).await {
            Some(p) => {
                return Some(p);
            }
            None => {
                log::debug!("tcp_connect_timeout={}", self.options.tcp_connect_timeout);
                match get_tcp_pool(
                    addr,
                    self.options.tcp_pool_size,
                    Duration::from_millis(self.options.tcp_connect_timeout),
                    Duration::from_secs(self.options.tcp_idle_timeout),
                )
                .await
                {
                    Some(p) => {
                        let mut tmp = self.pools.clone();
                        tmp.insert(addr.to_string(), p).await;
                        return self.pools.get(addr.to_string()).await;
                    }
                    None => {
                        return None;
                    }
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct WebhookHandler<'a> {
    packet: &'a packet::WebhookPacket,
    client: WebhookClient,
    client_id: String,
}

impl<'a> WebhookHandler<'a> {
    pub fn new(
        packet: &packet::WebhookPacket,
        client: WebhookClient,
        client_id: String,
    ) -> WebhookHandler {
        WebhookHandler {
            packet,
            client,
            client_id,
        }
    }

    pub async fn handle(&self) -> TaskResult {
        let result = self
            .client
            .send(
                &self.packet.address,
                &self.packet.payload,
                &self.packet.headers,
                self.packet.auth_required,
            )
            .await;

        match result {
            Ok(status_code) => {
                if status_code.is_success() {
                    TaskResult {
                        client_id: self.client_id.clone(),
                        result: format!("{}|0x00\r\n", self.packet.id),
                        error_detail: None,
                    }
                } else {
                    TaskResult {
                        client_id: self.client_id.clone(),
                        result: format!("{}|0x01\r\n", self.packet.id),
                        error_detail: Some(format!("Status >= 400 ({})", status_code)),
                    }
                }
            }
            Err(e) => {
                log::warn!("Couldn't sending webhook: {}", e);
                TaskResult {
                    client_id: self.client_id.clone(),
                    result: format!("{}|0x01\r\n", self.packet.id),
                    error_detail: Some(format!("{}", e)),
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct UdpHandler<'a> {
    packet: &'a packet::ProtocolPacket,
    pool: ConnectionPool<UdpConnectionManager>,
    client_id: String,
}

impl<'a> UdpHandler<'a> {
    pub fn new(
        packet: &packet::ProtocolPacket,
        pool: ConnectionPool<UdpConnectionManager>,
        client_id: String,
    ) -> UdpHandler {
        UdpHandler {
            packet,
            pool,
            client_id,
        }
    }

    pub async fn handle(&self) -> TaskResult {
        let mut res = String::new();
        let mut err = None;

        let destination = format!("{}:{}", self.packet.addr, self.packet.port);

        let socket = match self.pool.get().await {
            Ok(c) => c,
            Err(e) => {
                log::warn!("Couldn't get client for {}: {}", destination, e);
                err = Some(format!("No client: {}", e));
                res.push_str(&format!("{}|0x02\r\n", self.packet.id));

                return TaskResult {
                    client_id: self.client_id.clone(),
                    result: res.clone(),
                    error_detail: err,
                };
            }
        };

        match socket
            .send_to(self.packet.payload.as_bytes(), &destination)
            .await
        {
            Ok(_) => {
                res.push_str(&format!("{}|0x00\r\n", self.packet.id));
            }
            Err(e) => {
                log::warn!(
                    "UDP: Couldn't write to destination {}: {:?}",
                    destination,
                    e
                );
                err = Some(format!("Write failed: {:?}", e));
                res.push_str(&format!("{}|0x02\r\n", self.packet.id));
            }
        }

        TaskResult {
            client_id: self.client_id.clone(),
            result: res.clone(),
            error_detail: err,
        }
    }
}

pub struct EmailHandler<'a> {
    packet: &'a packet::EmailPacket,
    client: EmailClient,
    client_id: String,
}

impl<'a> EmailHandler<'a> {
    pub fn new(
        packet: &packet::EmailPacket,
        client: EmailClient,
        client_id: String,
    ) -> EmailHandler {
        EmailHandler {
            packet,
            client,
            client_id,
        }
    }

    pub async fn handle(&self) -> TaskResult {
        let mut res = String::new();
        let mut err = None;

        log::debug!("EmailPacket={:?}", self.packet);

        let attachments = self
            .packet
            .attachment
            .as_ref()
            .map_or(Vec::<String>::new(), |v| v.to_vec());
        let names = self
            .packet
            .attachment_name
            .as_ref()
            .map_or(Vec::<String>::new(), |v| v.to_vec());

        let result = self
            .client
            .send(
                &self.packet.email,
                &self.packet.subject,
                &self.packet.payload,
                &attachments,
                &names,
            )
            .await;

        match result {
            Ok(_) => TaskResult {
                client_id: self.client_id.clone(),
                result: format!("{}|0x00\r\n", self.packet.id),
                error_detail: err,
            },
            Err(e) => {
                match e {
                    ExporterError::AttachmentDecodeError(_) => {
                        res.push_str(&format!("{}|0x02\r\n", self.packet.id));
                        err = Some(format!("Bad attachment: {}", e));
                    }
                    _ => {
                        res.push_str(&format!("{}|0x01\r\n", self.packet.id));
                        err = Some(format!("Failed to send email: {}", e));
                    }
                }
                TaskResult {
                    client_id: self.client_id.clone(),
                    result: res.clone(),
                    error_detail: err,
                }
            }
        }
    }
}
