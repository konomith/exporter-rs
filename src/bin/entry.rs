use std::io;
use std::process;

use clap::{App, Arg};
use log;
use tokio::signal::unix::{signal, SignalKind};

use exporter;

fn get_matches() -> clap::ArgMatches<'static> {
    let max_packet_size = Box::leak(format!("{}", 1024 * 1024 * 64 * 2).into_boxed_str());
    let task_buff_size = Box::leak(format!("{}", 1024 * 128).into_boxed_str());
    let read_buffer_size = Box::leak(format!("{}", 1024 * 64).into_boxed_str());

    App::new("Exporter Server")
        .version(exporter::VERSION)
        .author("Theohar Konomi")
        .about("exporter server implementation in rust")
        .arg(
            Arg::with_name("host")
                .long("--host")
                .default_value("0.0.0.0")
                .help("The host address to bind to"),
        )
        .arg(
            Arg::with_name("port")
                .short("-p")
                .long("--port")
                .default_value("8888")
                .help("The port to bind to"),
        )
        .arg(
            Arg::with_name("no-wait")
                .long("--no-wait")
                .help("Don't wait for workers to finish pending tasks"),
        )
        .arg(
            Arg::with_name("log_level")
                .short("-l")
                .long("--log-level")
                .default_value("INFO")
                .help("The log level for the server"),
        )
        .arg(
            Arg::with_name("tcp-pool-size")
                .long("--tcp-pool-size")
                .default_value("10")
                .help("Number of pooled connections to tcp destinations"),
        )
        .arg(
            Arg::with_name("tcp-connect-timeout")
                .long("--tcp-connect-timeout")
                .default_value("1000")
                .help("Connection timeout for tcp destinations in milliseconds"),
        )
        .arg(
            Arg::with_name("tcp-idle-timeout")
                .long("--tcp-idle-timeout")
                .default_value("600")
                .help("Number of seconds to keep pooled tcp connections open when idle"),
        )
        .arg(
            Arg::with_name("tcp-workers")
                .long("--tcp-workers")
                .default_value("10")
                .help("Number of workers to handle tcp type packets"),
        )
        .arg(
            Arg::with_name("udp-pool-size")
                .long("--udp-pool-size")
                .default_value("1")
                .help("Number of pooled sockets to udp destinations"),
        )
        .arg(
            Arg::with_name("udp-idle-timeout")
                .long("--udp-idle-timeout")
                .default_value("600")
                .help("Number of seconds to keep pooled udp connections open when idle"),
        )
        .arg(
            Arg::with_name("udp-workers")
                .long("--udp-workers")
                .default_value("10")
                .help("Number of workers to handle udp type packets"),
        )
        .arg(
            Arg::with_name("webhook-workers")
                .long("--webhook-workers")
                .default_value("10")
                .help("Number of workers to handle webhook type packets"),
        )
        .arg(
            Arg::with_name("webhook-connect-timeout")
                .long("--webhook-connect-timeout")
                .default_value("2000")
                .help("Connection timeout for webhook destinations in milliseconds"),
        )
        .arg(
            Arg::with_name("webhook-skip-cert-verify")
                .long("--webhook-skip-cert-verify")
                .help("Don't verify certificates [Warning: Not recommended]"),
        )
        .arg(
            Arg::with_name("oauth-server")
                .long("--oauth-server")
                .default_value("http://localhost:8080")
                .help("The oauth server to connect to"),
        )
        .arg(
            Arg::with_name("oauth-realm")
                .long("--oauth-realm")
                .default_value("tinaa")
                .help("The oauth realm to use"),
        )
        .arg(
            Arg::with_name("oauth-client-id")
                .long("--oauth-client-id")
                .default_value("exporter")
                .help("The oauth client-id to use"),
        )
        .arg(
            Arg::with_name("oauth-client-secret")
                .long("--oauth-client-secret")
                .default_value("exporter")
                .help("The oauth client secret to use"),
        )
        .arg(
            Arg::with_name("task-buff-size")
                .long("--task-buff-size")
                .default_value(task_buff_size)
                .help("Bounded capacity of internal messaging channels"),
        )
        .arg(
            Arg::with_name("max-packet-size")
                .long("--max-packet-size")
                .default_value(max_packet_size)
                .help("Maximum size of a received packet (in bytes)"),
        )
        .arg(
            Arg::with_name("read-buffer-size")
                .long("--read-buffer-size")
                .default_value(read_buffer_size)
                .help("Maximum size of a received packet (in bytes)"),
        )
        .arg(
            Arg::with_name("email-workers")
                .long("--email-workers")
                .default_value("10")
                .help("Number of workers to handle email type packets"),
        )
        .arg(
            Arg::with_name("email-server")
                .long("--email-server")
                .default_value("localhost")
                .help("The smtp email server to connect to"),
        )
        .arg(
            Arg::with_name("email-port")
                .long("--email-port")
                .default_value("1025")
                .help("The smtp email port to connect to"),
        )
        .arg(
            Arg::with_name("email-username")
                .long("--email-username")
                .default_value("exporter@localhost")
                .help("The smtp email username"),
        )
        .arg(
            Arg::with_name("email-password")
                .long("--email-password")
                .default_value("")
                .help("The smtp email server password to use"),
        )
        .arg(
            Arg::with_name("email-skip-tls-verify")
                .long("--email-skip-tls-verify")
                .help("Don't verify smtp server tls certificates [Warning: Not recommended]"),
        )
        .arg(
            Arg::with_name("email-tls")
                .long("--email-tls")
                .help("Enable tls when connecting to the smtp server"),
        )
        .arg(
            Arg::with_name("email-auth")
                .long("--email-auth")
                .help("Mark authentication as required"),
        )
        .arg(
            Arg::with_name("email-from")
                .long("--email-from")
                .default_value("Exporter <exporter@localhost>")
                .help("The smtp from name to use"),
        )
        .arg(
            Arg::with_name("config")
                .long("--config")
                .help("Use the config file FILE")
                .value_name("FILE")
                .takes_value(true),
        )
        .get_matches()
}

async fn stop() -> io::Result<()> {
    let mut sigint_stream = signal(SignalKind::interrupt())?;
    let mut sigterm_stream = signal(SignalKind::terminate())?;
    tokio::select! {
        _sigint = sigint_stream.recv() => {
            log::info!("Caught SIGINT");
            Ok(())
        }
        _sigterm = sigterm_stream.recv() => {
            log::info!("Caught SIGTERM");
            Ok(())
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = get_matches();

    let log_level = args.value_of("log_level").unwrap();
    let _handle = exporter::logging::simple_config(log_level).unwrap();

    let opts = exporter::ServerOptions::from_matches(&args);
    let server = exporter::Server::new(opts);

    if let Err(err) = server.run(stop()).await {
        log::error!("Server encountered an error: {}", err);
        process::exit(1);
    }

    Ok(())
}
