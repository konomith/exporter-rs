use clap::{App, Arg};
use tokio::time;

use exporter::client::protocol::DefaultProtocolClient;

fn get_matches() -> clap::ArgMatches<'static> {
    App::new("Exporter Protocol Client Test")
        .version(exporter::VERSION)
        .author("Theohar Konomi")
        .arg(
            Arg::with_name("exporter_addr")
                .short("-e")
                .long("--exporter")
                .default_value("0.0.0.0:8888")
                .help("Exporter server to connect to"),
        )
        .arg(
            Arg::with_name("addr")
                .long("--addr")
                .default_value("0.0.0.0")
                .help("The addr to send to"),
        )
        .arg(
            Arg::with_name("port")
                .short("-p")
                .long("--port")
                .default_value("9001")
                .help("The port to send to to"),
        )
        .arg(
            Arg::with_name("PAYLOAD")
                .required(false)
                .help("The payload to send")
                .default_value("test"),
        )
        .arg(
            Arg::with_name("packets")
                .long("--packets")
                .default_value("10")
                .help("The number of packets to send"),
        )
        .get_matches()
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = get_matches();

    let exporter_addr = matches.value_of("exporter_addr").unwrap();
    let addr = matches.value_of("addr").unwrap();
    let port = matches.value_of("port").unwrap().parse::<u16>().unwrap();
    let payload = matches.value_of("PAYLOAD").unwrap();
    let packets = matches.value_of("packets").unwrap().parse::<u64>().unwrap();

    let client = DefaultProtocolClient::connect(exporter_addr).await?;

    for i in 0..packets {
        let id = client
            .send_tcp(addr, port, &format!("{}{}\n", payload, i))
            .await?;
        println!("Sent: {}", id);
    }

    for i in 0..packets {
        for _ in 0..10 {
            match client.check_output(i).await {
                Some(val) => {
                    println!("[{}] res={}", i, val);
                    break;
                }
                None => {
                    time::sleep(time::Duration::from_millis(200)).await;
                }
            }
        }
    }
    Ok(())
}
