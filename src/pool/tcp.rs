use std::io;
use std::net::{SocketAddr, ToSocketAddrs};
use std::time::Duration;

use async_trait::async_trait;
use bb8::PooledConnection;
use log;
use tokio::net::TcpStream;

#[derive(Debug)]
pub struct TcpConnectionManager {
    addr: SocketAddr,
}

impl TcpConnectionManager {
    pub fn new(addr: &str) -> Result<TcpConnectionManager, io::Error> {
        let mut addrs = addr.to_socket_addrs()?;
        match addrs.next() {
            Some(addr) => Ok(TcpConnectionManager { addr }),
            None => Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                "Invalid address",
            )),
        }
    }
}

#[async_trait]
impl bb8::ManageConnection for TcpConnectionManager {
    type Connection = TcpStream;
    type Error = io::Error;

    async fn connect(&self) -> Result<Self::Connection, Self::Error> {
        let s = TcpStream::connect(self.addr).await?;
        Ok(s)
    }

    async fn is_valid(&self, conn: &mut PooledConnection<'_, Self>) -> Result<(), Self::Error> {
        let mut buf = [0; 1];
        match conn.try_read(&mut buf) {
            Ok(read) => {
                log::debug!("is_valid: try_read: OK: {}", read);
                if read == 0 {
                    return Err(io::Error::new(io::ErrorKind::Other, "Remote disconnected"));
                }
                Ok(())
            }
            Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                log::debug!("is_valid: wouldblock");
                Ok(())
            }
            Err(e) => {
                log::debug!("is_valid: err: {}", e);
                Err(e)
            }
        }
    }

    fn has_broken(&self, conn: &mut Self::Connection) -> bool {
        let mut buf = [0; 1];
        match conn.try_read(&mut buf) {
            Ok(read) => {
                log::debug!("has_broken: try_read: OK: {}", read);
                if read == 0 {
                    return true;
                }
                false
            }
            Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                log::debug!("has_broken: wouldblock");
                false
            }
            Err(e) => {
                log::debug!("has_broken: err: {}", e);
                true
            }
        }
    }
}

pub async fn get_tcp_pool(
    addr: &str,
    max_size: u32,
    timeout: Duration,
    idle_timeout: Duration,
) -> Option<bb8::Pool<TcpConnectionManager>> {
    let manager = TcpConnectionManager::new(addr);

    if manager.is_err() {
        return None;
    }

    let manager = manager.unwrap();

    match bb8::Pool::builder()
        .max_size(max_size)
        .connection_timeout(timeout)
        .idle_timeout(Some(idle_timeout))
        .build(manager)
        .await
    {
        Ok(pool) => Some(pool),
        Err(_) => None,
    }
}
