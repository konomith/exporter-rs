use std::io;
use std::time::Duration;

use async_trait::async_trait;
use bb8::PooledConnection;
use log;
use tokio::net::UdpSocket;

#[derive(Debug)]
pub struct UdpConnectionManager;

#[async_trait]
impl bb8::ManageConnection for UdpConnectionManager {
    type Connection = UdpSocket;
    type Error = io::Error;

    async fn connect(&self) -> Result<Self::Connection, Self::Error> {
        let s = UdpSocket::bind("0.0.0.0:0").await?;
        log::debug!("Created udp socket: {:?}", s);
        Ok(s)
    }

    async fn is_valid(&self, _conn: &mut PooledConnection<'_, Self>) -> Result<(), Self::Error> {
        Ok(())
    }

    fn has_broken(&self, _conn: &mut Self::Connection) -> bool {
        false
    }
}

pub async fn get_udp_pool(
    max_size: u32,
    idle_timeout: Duration,
) -> Option<bb8::Pool<UdpConnectionManager>> {
    let manager = UdpConnectionManager;

    match bb8::Pool::builder()
        .max_size(max_size)
        .idle_timeout(Some(idle_timeout))
        .build(manager)
        .await
    {
        Ok(pool) => Some(pool),
        Err(_) => None,
    }
}
