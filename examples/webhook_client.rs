use exporter::client::webhook::DefaultWebhookClient;
use std::collections::HashMap;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let c = DefaultWebhookClient::connect("localhost:8888").await?;
    c.send(
        "http://localhost:9000",
        &HashMap::new(),
        "this is the body",
        false,
    )
    .await?;
    tokio::time::sleep(std::time::Duration::from_secs(1)).await;
    Ok(())
}
