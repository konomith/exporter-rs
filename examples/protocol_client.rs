use exporter::client::protocol::DefaultProtocolClient;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let c = DefaultProtocolClient::connect("localhost:8888").await?;
    c.send_tcp("localhost", 9001, "test\n").await?;
    tokio::time::sleep(std::time::Duration::from_secs(1)).await;
    Ok(())
}
